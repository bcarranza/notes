

## Use it.

```shell
podman machine start
```


```shell
podman run -p 8088:80 kennethreitz/httpbin
```

```
podman run  --rm -it ubuntu:latest /bin/bash 
```


## Make it work.

See [this comment](https://github.com/containers/podman/issues/16470#issuecomment-1340997755). 


```
This machine is currently configured in rootless mode. If your containers
require root permissions (e.g. ports < 1024), or if you run into compatibility
issues with non-podman clients, you can switch using the following command:

	podman machine set --rootful
```