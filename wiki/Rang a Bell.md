> I collect links to things that rang a bell for me. These are things that may not be useful everyday but may be super useful on the day that they are needed. 


Run `touch /etc/gitlab/skip-auto-backup` to get GitLab to skip taking a backup during upgrade. 

## Issues

- [Sidekiq keeps restarting after upgrade to 17.0.0](https://gitlab.com/gitlab-org/omnibus-gitlab/-/issues/8545)


## GitLab Pages

  Docs: [Migrate legacy storage to ZIP storage](https://docs.gitlab.com/ee/administration/pages/#migrate-legacy-storage-to-zip-storage)


> GitLab tries to [automatically migrate](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/54578) the old storage format to the new ZIP-based one when you upgrade to GitLab 13.11 or further.


## GitLab Runner

```
curl -s http://localhost:9402/debug/jobs/list
```

[source](https://gitlab.com/gitlab-org/gitlab-runner/-/issues/27093#note_793812818)

## Authentication

See [supported user attributes](https://docs.gitlab.com/ee/user/group/saml_sso/index.html#supported-user-attributes). Those attributes take effect for users created after the attribute is set. In other words: the value is not toggled retroactively. It's queried when the user is created. 

`can_create_group` and `projects_limit`


### SAML

See [the docs](https://docs.gitlab.com/ee/integration/saml.html#use-multiple-saml-identity-providers). 

- Each provider has a unique name set that matches a name set in `args`. At least one provider **must** have the name `saml` to mitigate a [known issue](https://gitlab.com/gitlab-org/gitlab/-/issues/366450?_gl=1*fp5xm*_ga*MTQ2MDM2MjM4Ni4xNjU0Njk3OTUy*_ga_ENFH3X7M5Y*MTY2NDgwMDgxNS4xMzYuMC4xNjY0ODAwODE1LjAuMC4w) in GitLab 14.6 and newer.
- 


### Unauthenticated connection first

When performing `git` over `HTTP` operations, the `git` client will attempt an _unauthenticated_ connection first. Enable `GIT_TRACE` to see this on the client side. 

```console
GIT_TRACE=true git whatever
```


In the logs, you'llsee:

```
10.10.1-.84 - - [26/Aug/2022:03:30:30 +0000] "GET /cats/whatever.git/info/refs?service=git-receive-pack HTTP/2.0" 401 26 "" "git/2.36.1" -
10.10.10.84 - cats [26/Aug/2022:03:30:42 +0000] "GET /cats/whatever.git/info/refs?service=git-receive-pack HTTP/2.0" 401 26 "" "git/2.36.1" -
```

Docs: [Debugging with Git embedded traces](https://docs.gitlab.com/ee/topics/git/useful_git_commands.html#debugging-with-git-embedded-traces) 

Check the [Debugging](https://git-scm.com/book/en/v2/Git-Internals-Environment-Variables) section of the linked page from the official `git` documentation. 


  - [ ] I _believe_ that Docker does this, too. Check it out.


  - With AKS, load balancer may take a while: requests will return with error for _minutes_ after. This is not unique to AKS but we seem to hear more about it with AKS. 
https://learn.microsoft.com/en-us/windows/win32/secauthn/protocols-in-tls-ssl--schannel-ssp-#tls-protocol-version-support



## PostgreSQL

`\d` is `DESCRIBE`

I'm used to doing `DESCRIBE uploads;` in order to get the layout of a table. Unfortunately, `postgres` doesn't know what I want when I ask for this. The thing I am looking for is `\d` (and not `\dt`).


```
gitlabhq_production=# DESCRIBE uploads;
ERROR:  syntax error at or near "DESCRIBE"
LINE 1: DESCRIBE uploads;
```

lolwut


```
gitlabhq_production=# \d uploads
                                         Table "public.uploads"
   Column    |            Type             | Collation | Nullable |               Default
-------------+-----------------------------+-----------+----------+-------------------------------------
 id          | integer                     |           | not null | nextval('uploads_id_seq'::regclass)
 size        | bigint                      |           | not null |
 path        | character varying(511)      |           | not null |
 checksum    | character varying(64)       |           |          |
 model_id    | integer                     |           |          |
 model_type  | character varying           |           |          |
 uploader    | character varying           |           | not null |
 created_at  | timestamp without time zone |           | not null |
 store       | integer                     |           |          | 1
 mount_point | character varying           |           |          |
 secret      | character varying           |           |          |
Indexes:
    "uploads_pkey" PRIMARY KEY, btree (id)
    "index_uploads_on_checksum" btree (checksum)
    "index_uploads_on_model_id_and_model_type" btree (model_id, model_type)
    "index_uploads_on_store" btree (store)
    "index_uploads_on_uploader_and_path" btree (uploader, path)
Check constraints:
    "check_5e9547379c" CHECK (store IS NOT NULL)
Referenced by:
    TABLE "upload_states" CONSTRAINT "fk_rails_d00f153613" FOREIGN KEY (upload_id) REFERENCES uploads(id) ON DELETE CASCADE
```

