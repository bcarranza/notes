# Code Contributions

In late 2023, I started focusing on code contributions with an eye towards the [analyzers](https://gitlab.com/gitlab-org/security-products/analyzers) in `gitlab-org/security-products/analyzers`.

## Local Testing and Development

[Analyzer Common README](https://gitlab.com/gitlab-org/secure/tools/analyzer-scripts/-/blob/master/analyzers-common-readme.md)


```
git clone git@gitlab.com:gitlab-org/secure/tools/analyzer-scripts.git
```


```
podman login registry.gitlab.com
```

## DAST Authentication Report

When inspecting a particular `gl-dast-debug-auth-report.html` generated during a `dast` job, I noticed that the **DOM** appeared all on one line. The [example](https://docs.gitlab.com/ee/user/application_security/dast/authentication_troubleshooting.html#configure-the-authentication-report) in the docs has the **DOM** displayed...differently, not all on one line.

  - [ ] Why does the **DOM** _sometimes_ appear all on one line?
    - Is this because of a user-controlled setting?
    - Can I get it to appear all on one line on purpose?
    - Can we account for this and improve the auth report? Should we?
  - [ ] How are we retrieving and storing the **DOM**?

### Headers for sites with nice DOMs

```
HTTP/1.1 404 Not Found
CF-Cache-Status: DYNAMIC
CF-Ray: 84eca788cc6c0812-IAD
Connection: keep-alive
Content-Encoding: gzip
Content-Length: 690
Content-Type: text/html; charset=utf-8
Date: Thu, 01 Feb 2024 19:38:23 GMT
Server: cloudflare
Vary: Accept-Encoding
alt-svc: h3=":443"; ma=86400
rndr-id: c588fc63-271e-4d84
x-render-origin-server: gunicorn
``

## Reference

- [Introduction to the DOM](https://developer.mozilla.org/en-US/docs/Web/API/Document_Object_Model/Introduction)
