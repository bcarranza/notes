# Git LFS and GitLab

> Storing Large Files

## Quickstart

  1. Ensure that you have installed the appropriate `git-lfs` package for your system
  1. `git lfs install`
  1. `git lfs track "*.cat"`
  1. `git add cute.cat`
  1. `git add .gitattributes`


Commit and push! 



> Files tracked by Git LFS display an icon to indicate if the file is stored as a blob or an LFS pointer.



## Docs

  - Using [Git LFS](https://docs.gitlab.com/ee/topics/git/lfs/index.html)
  - [Git LFS](https://git-lfs.github.com/)


## LFS Administration

```
gitlab-rake  gitlab:lfs:check
```


If everything is going well, the output looks like this:

```
# gitlab-rake  gitlab:lfs:check
^[[A
Checking integrity of LFS objects
- 1..5: Failures: 0
Done!
```