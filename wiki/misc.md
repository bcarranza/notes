```
 mysqlconnectionid="$(docker run -d \\n -e MYSQL_RANDOM_ROOT_PASSWORD=true \\n -e MYSQL_DATABASE=eicar \\n -e MYSQL_USER=racie \\n -e MYSQL_PASSWORD=secret \\n "mysql:5.7")"
```


## Questions

- Was I on the right path for validating a digital certificate in Windows with Powershell?
I was thinking about a few options:

- [Invoke-WebRequest](https://learn.microsoft.com/en-us/powershell/module/microsoft.powershell.utility/invoke-webrequest?view=powershell-7.4)
	- I want to do this _without_ `.NET` -- back in 2016 that wasn't possible per [Assessing Remote Certificates with Powershell](https://isc.sans.edu/forums/diary/Assessing+Remote+Certificates+with+Powershell/20645/). According to this 2020 blog post, that may no longer be true: [Display Certificate info of remote HTTPS connection](https://nickeales.github.io/2020-08-25-Display-Certificate-info-of-remote-https-connection.html).
	- Yep. I think that `Test-Certificate` is close to what I want but I want the command I use to go ahead and _retrieve_ the certificate for me. This cmdlet wants me to supply it. [src](https://learn.microsoft.com/en-us/powershell/module/pki/test-certificate?view=windowsserver2022-ps)
		- But we can probably workaround that with `Get-childitem cert: -recurse` or similar.
- Down the rabbit hole
	- [About HTTPS, SChannel, TLS, CAPI, SSL Certificates and their keys](https://techcommunity.microsoft.com/t5/iis-support-blog/about-https-schannel-tls-capi-ssl-certificates-and-their-keys/ba-p/815200)
	- [UsingTrustedRootsRespectfully](https://www.mono-project.com/archived/usingtrustedrootsrespectfully/)
