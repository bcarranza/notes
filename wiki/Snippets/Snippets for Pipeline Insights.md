These are snippets about features associated with [[Pipeline Insights]].


```
# Let's get one instance of a build that they expect to have coverage based on the job trace
> job_id = <pick an ID of Job that is expected to have coverage>
> build = Ci::Build.find job_id
> coverage = build.trace.extract_coverage(build.coverage_regex)
```




If that doesn't return anything, look further:

```
# Let's check if coverage_regex has a value
> build.coverage_regex
# Let's also check if trace is present and what it contains
> build.trace.raw
```



