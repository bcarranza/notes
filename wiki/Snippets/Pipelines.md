Delete all skipped pipelines.


```
gitlab-rails runner "skipped = Ci::Pipeline.skipped; skipped.find_each do |pipe|; pipe.delete; end"
```


[source](https://gitlab.com/gitlab-org/gitlab/-/issues/28369#note_595324031)