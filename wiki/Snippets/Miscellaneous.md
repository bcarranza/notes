# Miscellaneous Snippets for the GitLab Rails Console


That looks cool! 

```ruby
project = Project.find(ID)
Gitlab::Kas::Client.new.list_agent_config_files(project: project)
```

Looking at environment variables:

```ruby
ENV.key?('no_proxy')
ENV['no_proxy']
ENV
```


```ruby
prj=Project.find_by_full_path("hello/world");
prj.container_expiration_policy.update_attribute(:enabled, true);
prj.container_expiration_policy.save;
```

Move repository storages while bypassing Sidekiq:

```ruby
user_project = Project.find(9)
storage_move = user_project.repository_storage_moves.create!(source_storage_name: user_project.repository_storage, destination_storage_name: 'storage-2')
storage_move.container.set_repository_read_only!(skip_git_transfer_check: true)
storage_move.state = 2
storage_move.save!
storage_move.reload

Projects::UpdateRepositoryStorageService.new(storage_move).execute
```


How many repos are in a `read-only` state?

```ruby
Project.where(repository_read_only: true).count
```

How many repos are on a storage called `default`?

```ruby
Project.where(repository_storage: "default").count
```


Is it `read-only` and you don't want it to be?

```ruby
Project.find(id).update!(repository_read_only: false)
```

### Application Settings

To speed up the execution of batched background migrations, two migrations are executed at the same time.

[GitLab administrators with access to the GitLab Rails console](https://docs.gitlab.com/ee/administration/feature_flags.html) can change the number of batched background migrations executed in parallel.

What's the current value?

```ruby
 ApplicationSetting.current.database_max_running_batched_background_migrations
```

Adjust the value from the default of `2` to `8`:

```ruby
ApplicationSetting.update_all(database_max_running_batched_background_migrations: 8)
```

```ruby
Gitlab::CurrentSettings.database_max_running_batched_background_migrations
```


## Snippets

These are things that are in actual [snippets](https://docs.gitlab.com/ee/user/snippets.html) on `gitlab.com`:

- [ℹ \[GitLab\] Check whether 2FA is enforced](https://gitlab.com/-/snippets/3634851) for an entire instance
- You can browse through [all of my snippets](https://gitlab.com/users/bcarranza/snippets). 
