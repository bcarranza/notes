

- https://gitlab.com/gitlab-org/security-products/demos/dast

## Ingredients

- [Services](https://docs.gitlab.com/ee/ci/services/)



##  💡 Ideas


- [ ] https://www.freecodecamp.org/news/end-to-end-api-testing-with-docker/
- [ ] https://github.com/beevelop/docker-nginx-basic-auth
- [ ] https://ahmet.im/blog/docker-http-basic-auth/
- [ ] https://dev.to/kevinlien/password-protect-a-dockerized-nginx-server-step-by-step-3oie
- [ ] https://haseebmajid.dev/posts/2022-08-08-running-gitlab-ci-jobs-in-docker-using-docker-compose/