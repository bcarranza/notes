# ✨ Triage Ops ✨

## 📋 Prep 📋

Possibly helpful `localhost` setup:

```shell
# cat ~/.gemrc                    
gem: --user-install
```

```shell
git clone git@gitlab.com:gitlab-org/quality/triage-ops.git
gem install gitlab-triage
```

Make sure `$PATH` includes `/home/bcarranza/.local/share/gem/ruby/3.0.0/bin` or similar.



```shell 
gitlab-triage --init
```

## ✍️ Writing the Policy ✍️

The `triage-ops` reference on [fields](https://gitlab.com/gitlab-org/ruby/gems/gitlab-triage#fields) will likely be helpful as will existing policies.

## 👩‍🍳 Ingredients 👩‍🍳

- [testing](https://gitlab.com/gitlab-org/quality/triage-ops/-/blob/master/doc/scheduled/index.md#testing-a-schedule-with-a-dry-run-pipeline)
- [running locally](https://gitlab.com/gitlab-org/quality/triage-ops/-/blob/master/doc/scheduled/index.md#locally)
