##  Deploying a Docker test environment
I use Docker Compose exclusively. I _love_ [Docker Compose](https://github.com/docker/compose):

> Define and run multi-container applications with Docker

That's just what I want! 

I have [example configs](https://gitlab.com/bcarranza/configs) in the `bcarranza/configs` project. 

  - This [simple setup](https://gitlab.com/bcarranza/configs/-/blob/main/docker-compose.yml) is a single Omnibus and a single Runner with configs for LDAP, SMTP, et cetera. 
  - This [example](https://gitlab.com/bcarranza/configs/-/blob/main/external-postgresql/docker-compose.yml) starts to demonstrate how to do external Redis and external PostgreSQL. 


  - [ ] Do we haave LDAP in the docs?


##  Using Docker locally

Given the wonder that is `gitlabsandbox.cloud` , I use Docker a bit differently these days. I now use [[Podman]] for much of what I used Docker for previously. 

  - [x] I am on very low priority poking at the [Podman with docker-compose on MacOS.](https://gist.github.com/kaaquist/dab64aeb52a815b935b11c86202761a3) gist. More about my experience with [[Podman]]. 


##  Common GitLab in Docker "gotchas"/pitfalls


### Configuration

I do something like this:

`start.sh`

```
RUNNER_HOME=/srv/gitlab/runner GITLAB_HOME=/srv/gitlab docker-compose up -d
```

`rebuild.sh`

```
RUNNER_HOME=/srv/gitlab/runner GITLAB_HOME=/srv/gitlab docker-compose up  --build -d
```

You can set the `hostname` in `docker-compose.yml` and set up DNS in `/etc/hosts` or whatever. 

### Maintenance
One frustrating thing about using Docker Compose is having to deal with [different versions of the compose file](https://docs.docker.com/compose/compose-file/compose-versioning/). 

Another thing to keep in mind: don't edit `/etc/gitlab/gitlab.rb` manually! (The changes won't be preserved when the container is recreated. )

##  Miscellaneous

Docs: [GitLab Docker images](https://docs.gitlab.com/ee/install/docker.html)

We offer guidance on [using Docker Engine, Compose or swarm mode](https://docs.gitlab.com/ee/install/docker.html#installation). 



### Commands


```
today && mkdir config data logs && \
docker run --detach --hostname gl2.example.net --publish 4443:443 --publish 8088:80 --publish 2222:22 --name gitlab17 --restart unless-stopped --volume ./config:/etc/gitlab --volume ./logs:/var/log/gitlab --volume ./data:/var/opt/gitlab --memory=4g --cpus=3.5 --shm-size=256m gitlab/gitlab-ee:17.7.2-ee.0
```


---


  - [ ] In onboarding, tell people how to find `gitlab.rb` 