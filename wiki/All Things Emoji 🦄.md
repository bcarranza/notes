✨🍎 On macOS, use `Cmd` + `Ctrl` + `Space` to bring up the emoji picker.  🍎✨

![](img/emoji-picker.png)


Or try the **FREE** [🚀 Rocket](https://matthewpalmer.net/rocket/) app, which lets you use [emoji shortcodes]() (like `:cat:`) right on your machine. 

  - [[That Emoji Means]]

---


> Where user input is permitted, try an emoji. 
> --Brie


## Does that input field accept emoji?

One thing to keep in mind when testing this is that there are a few different levels of support for emoji:

  - The thing can be made entirely of emoji
  - The thing can include emoji
	  - the thing can include emoji but can't begin (or end) with emoji
- the thing can't include emoji at all
	- the error message is emoji-aware
	- the error message is not emoji-aware

### GitLab

You can use emoji all over the place! A few favorites:

  - Use an emoji for `stage` names in `.gitlab-ci.yml`
  - Use one or more emoji for a branch name
  - In a filename (like this one!)
	  - when [deployed to a GitLab Pages site](https://notes.brie.dev/All%20Things%20Emoji%20%F0%9F%A6%84/)  -- [[All Things Emoji 🦄]]

### Passwords

In 2019, I conducted an incomplete survey of support for emoji passwords on popular websites. 

**Fifty-two (52%) percent of the websites studied allow a password to be completely comprised of emoji.**

### Is it a bad idea to use a password that contains (or is completely comprised of) emoji?

🤷 It depends!

I did not test whether accounts with emoji-full passwords experience trouble with 2FA, password resets, mobile login, receiving password hints, et cetera. 

  - Maybe don't do this for anything mission-critical.  

#### Password Generators that Support Emoji

  - https://www.kamogo.com/16
  - https://www.sethserver.com/unicode-random-password-generator.html
  - https://ae7.st/g/
  - https://github.com/agentgt/ezpwdgen









## Miscellaneous

  - [Emoji Traffic Jam Generator](https://emoji-jam-rjlxe.ondigitalocean.app/)! 
  - [Emojibase](https://emojibase.dev)
  - [HEX/HTML Dec codes for emoji](https://dev.to/rodrigoodhin/list-of-emojis-hex-codes-35ma) 


## Suggestions for emoji in commit messages

Several folks have thoughts on this.

  - [gitmoji](https://gitmoji.dev/) - An emoji guide for your commit messages.
  - [Emoji-Log](https://github.com/ahmadawais/Emoji-Log) - An Emoji Git commit log messages spec standard. 
  - [Commit Message Emoji](https://github.com/dannyfritz/commit-message-emoji)
  - [GitCommitEmoji.md](https://gist.github.com/parmentf/035de27d6ed1dce0b36a)
  - [Why I use emojis in my Git commits](https://dev.to/rmarting/why-i-use-emojis-in-my-git-commits-4c10)

Utilities are better than thoughts!

  - [gitmoji-cli](https://github.com/carloscuesta/gitmoji-cli)
  - My personal favorite: [gitmoji-commit-hook](https://www.npmjs.com/package/gitmoji-commit-hook)


#### Up and running with `gitmoji-commit-hook`


This requires `npm` so there's that. Anyway:

```shell
npm install -g gitmoji-commit-hook
```


In the root of whatever repository you want to use `gitmoji-commit-hook` in run:

```shell
gitmoji-commit-hook --init
```


  - 👍 Emoji in every commit. 
  - 👎 This is an interactive tool so it will slow you down. 


## Mashing up Emoji

  - [Emoji Kitchen](https://emojikitchen.dev/)
  - [Emoji Supply Kitchen](https://emoji.supply/kitchen/)

As of [December 2020](https://blog.google/products/android/emoji-kitchen-new-mashups-mixing-experience/), the [Emoji Kitchen](https://emojikitchen.dev) has 14,000 combinations available. 

Browsing to [https://emoji.kitchen](https://emoji.kitchen), will redirect you to [@emojikitchen](https://twitter.com/emojikitchen). 

https://blog.google/products/android/feeling-all-the-feels-theres-an-emoji-sticker-for-that/


## Emoji Combos

💃🏻🌺🦄🎭

🍒🍄🌈🦋🦄🍰🎨🎀✨

  - [Aesthetic Emoji](https://www.aestheticemoji.com/)
	  - ✨🍄🌱
	  - 🍵🌵🍐🍈🌿🍀🌳
	  - 💎💙🌊💦🌧🐟
	  - 👜☕🕰🤎📜✒️✌🏽🧸🚪🧺🎻🥥🌰🏹⏳


On busier days, I create an issue to track what I want to do that day (or for the rest of that day). I use one of these as the title:

  - 🌻🍯🍋 Magical Monday 🍋🍯🌻
  - ✨🔭🌙 Terrific Tuesday 🌙🔭✨
  - 🍏🍓🥝 Wonderful Wednesday 🍓🥝🍏
  - 💌🗺️📎 Tranquil Thursday 📎🗺️💌
  - 🌈​🦄​🦋 Fantastic Friday 🦋🦄​🌈


## Beyond

### Custom Emoji for Slack (or whatever)

  - [Slackmojis](https://slackmojis.com/) - THE [unofficial*](https://slackmojis.com/#footer) directory of the best custom Slack emojis
  - [emoji.gg](https://emoji.gg/) - Emojis for Discord/Slack/Teams/whatever. Check out the [Custom Emoji Maker](https://emoji.gg/maker)! I made this with it:
![concerned-cat.png](concerned-cat.png)





### Open Source Emoji

  - [OpenMoji](https://openmoji.org/) - Open source emojis for designers, developers and everyone else!
  - Ooh, the [Open-source Emoji Pack for Devs](https://emoji.openess.dev/)! A few selections from that set:



![database-green.png](database-green.png)

![offline-purple.png](offline-purple.png)
