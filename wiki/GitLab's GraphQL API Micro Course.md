As a GitLab team member, I am taking the micro course on [GitLab's GraphQL API](https://levelup.gitlab.com/learn/course/gitlabs-graphql-api/main/gitlabs-graphql-api?client=internal-team-members). These are my notes. 

GraphQL - (data) Graph Query Language

  - YouTube: https://www.youtube.com/watch?v=OSlNc7F8-LU



Problems with REST:

  - Overfetching: the response returns _way_ more info than we need. 
  - Underfetching: the endpoint you use doesn't have all the info you need, more requests are required. 
  - We have to rely on the documentation (or just making a failing request and finding out) in order to figure out what the API should return, you have to be aware of all use cases. 




To work with GraphQL:

  1. Describe your data
  2. Ask for what you want
  3. Get predictable results

