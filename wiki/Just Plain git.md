
I need a place for just plain `git` stuff. There might not be much but here are a few scribbles:


### Unstage everything


```bash
Changes to be committed:
  (use "git restore --staged <file>..." to unstage)
```

Oops. I am **not** running `git restore --staged` and enumerating every file that I _accidentally_ did `git add` on, thank you! Instead, I'll be smart and do `git reset` and now `git status` is back to how I wanted it:

```bash
Untracked files:
  (use "git add <file>..." to include in what will be committed)
	../
```