# 💡 Contributing to the Knowledge Base

- See `knowledge-base/` in the [Support Pages](https://gitlab.com/gitlab-com/support/support-pages) project.

```shell
git@gitlab.com:gitlab-com/support/support-pages.git
```
