# 🏴‍☠️ Feature Flags


...beyond `Feature.enabled?`.

```
SELECT * FROM features;
```

```
gitlab-psql --csv -c 'SELECT * FROM features;'
```
