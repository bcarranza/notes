# vim

- `$` | end of line
- `:$` | end of file
- `:vvVVGd` | delete from here to the end of the file
