# 🐘 PostgreSQL

## The Basics

- **MVCC** multiversion concurrency control

## 📝 Training Notes

> These are what I most frequently want to link to...

- [PostgreSQL Training: Part 1 // 2021 - HackMD](https://hackmd.io/2bHfFb8dScWioiNZ8L3wIw)
- [PostgreSQL Training: Part 2 // 2021 - HackMD](https://hackmd.io/c2ZPSoj-SkqIMqlt50uiJQ)

---

## 📆 Late 2024

- [Beta test PostgreSQL locks training](https://gitlab.com/gitlab-com/support/support-team-meta/-/issues/6374)

### High Performance PostgreSQL

This text focuses on `OLTP` (online transaction processing) given the focus on short duration queries running in high volume and with high concurrency. This is instead of `OLAP` (online analytical processing).

The text uses the [andyatkinson/rideshare](https://github.com/andyatkinson/rideshare.git) app.

Active Record follows the `Convention over Configuration` design principle.


#### ERD

- [Create an Entity Relationship Diagram automatically for each release](https://gitlab.com/gitlab-org/database-team/team-tasks/-/issues/108)
- 

#### Glossary

- **CTE** Common Table Expressions

## 📚 Recommendable Reading

- [ ] [The Internals of PostgreSQL](https://www.interdb.jp/pg/)
