# GitLab Environment Toolkit

> GETxCNH

## Prereqs

- Ansible
- Helm
- Terraform

## Deployment Notes

Don't call the bucket `<envname>-terraform-state`!

Helm requires a hostname that resolves.

The load balancer hostname _may_ change between deployments. (Everything else is stable.)
