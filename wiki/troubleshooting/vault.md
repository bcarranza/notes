# Hashicorp Vault



## Bookmarks

  - [Awesome tools around HashiCorp Vault](https://github.com/gites/awesome-vault-tools)
### GitLab Docs
Best: https://docs.gitlab.com/ee/ci/examples/authenticating-with-hashicorp-vault/
OK: https://docs.gitlab.com/ee/ci/secrets/


### Write-Ups

  - [Securing HashiCorp Vault with Let’s Encrypt SSL](https://dwdraju.medium.com/securing-hashicorp-vault-with-lets-encrypt-ssl-19cad1eb294)


## Troubleshooting


  - [ ] Could it be a Rate Limit Quota is being met? | **READ** [Vault: Resource Quotas](https://learn.hashicorp.com/tutorials/vault/resource-quotas)
  - [ ] Check the value of the TTL on the role that is in use. 