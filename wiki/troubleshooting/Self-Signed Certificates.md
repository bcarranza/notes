

### Subject Alternative Name (SAN)

- [Know about SAN Certificate and How to Create With OpenSSL](https://geekflare.com/san-ssl-certificate/)

Do you have the `SAN`?

```
openssl s_client -connect example.com:443 </dev/null 2>/dev/null | openssl x509 -noout -text | grep DNS:
```

[source](https://gitlab.com/gitlab-org/gitlab-runner/-/issues/28841#note_1004765355)

## Generators

- `mkcert.dev` - [A simple zero-config tool to make locally trusted development certificates with any names you'd like.](https://github.com/FiloSottile/mkcert)
- [OMGWTFSSL - Self Signed SSL Certificate Generator](https://github.com/paulczar/omgwtfssl#omgwtfssl---self-signed-ssl-certificate-generator)
- XCa

### 🏆 `mkcert`

```
apt update && apt install -y golang-go &&  git clone https://github.com/FiloSottile/mkcert && cd mkcert &&   go build -ldflags "-X main.Version=$(git describe --tags)"
```


### OMGWTFSSL


```
podman run -e SSL_SUBJECT="registry.brie.lol" -e SSL_DNS="registry.brie.lol" -e SSL_SUBJECT="registry.brie.lol"   paulczar/omgwtfssl
```


The certs that are printed to `STDOUT` contain annoying leading spaces:

```
sed -i  -e 's/^[ \t]*//' registry.brie.lol.crt
sed -i  -e 's/^[ \t]*//' registry.brie.lol.key
```




