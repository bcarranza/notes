

1. Optionally create a `.yml` file like [this one](https://docs.gitlab.com/ee/user/group/manage.html#example-configuration)
2. Create the [compliance framework](https://docs.gitlab.com/ee/user/group/manage.html#compliance-frameworks) in the UI for the group. This can _not_ be done at the subgroup level. 
3. Optionally add the `.yml` file that you created in step one, with a path like `.compliance-gitlab-ci.yml@gitlab-gold/briecarranza/secure/compliant/the-file`. 
4. Add the framework.


OK, you've know configured the compliance framework and a compliance pipeline associated with it. We want to apply this to a project now. You have to [add the compliance framework](https://docs.gitlab.com/ee/user/project/settings/index.html#add-a-compliance-framework-to-a-project) to the project from the **Settings** for that project. 

Choose the framework from the list and click **Save changes**. 

When you browse to the project, you'll see a label. 

Example: [https://gitlab.com/gitlab-gold/briecarranza/secure/compliant/scanning-dependencies](https://gitlab.com/gitlab-gold/briecarranza/secure/compliant/scanning-dependencies)


![compliance-label.png](compliance-label.png)