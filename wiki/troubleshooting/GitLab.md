```
apt install gitlab-ee=15.1.6-ee.0
```


  - [ ] **TODO** Link to the upgrade tool


## Log Parsing

```
jq 'select(.status == 401)' production_json.log
```


See [[GitLab One Liners]].

Docs: [Parsing GItLab logs with `jq`](https://docs.gitlab.com/ee/administration/logs/log_parsing.html)


## Upgrading


Install a specific version:

```
apt install gitlab-ee=15.1.6-ee.0
```


---

# SaaS and Self-Managed

## SaaS

See [GitLab.com settings](https://docs.gitlab.com/ee/user/gitlab_com/#gitlab-cicd). This is helpful for understanding the value of instance-level settings on `gitlab.com`. 