## 🆕 What has changed?


- [CI deprecation finder](https://gitlab.com/gitlab-com/cs-tools/gitlab-cs-tools/deprecation-migration-tools/ci-deprecation-finder)
- [Deprecations and removals by version](https://docs.gitlab.com/ee/update/deprecations.html?removal_milestone=17.0#deprecations-and-removals-by-version) (`17.0`)
	- [A guide to the high-impact breaking changes in GitLab 17.0](https://about.gitlab.com/blog/2024/04/10/a-guide-to-the-high-impact-breaking-changes-in-gitlab-17-0/)

- [Engineering Projects](https://handbook.gitlab.com/handbook/engineering/projects/)


## 🔎 What might be wrong?

- [Linux Performance Tools](https://brendangregg.com/linuxperf.html)
