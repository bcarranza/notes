
> Owners of code. 

 [Require Code Owner approval on a protected branch](https://docs.gitlab.com/ee/user/project/protected_branches.html#require-code-owner-approval-on-a-protected-branch) -- the default branch or another.

## Docs

> What do the docs say?

The [Groups as Code Owners](https://docs.gitlab.com/ee/user/project/code_owners.html#groups-as-code-owners) documentation says:

> Members in the Code Owners group also must be direct members, and not inherit membership from any parent groups.


This might require clarification. The [troubleshooting](https://docs.gitlab.com/ee/user/project/code_owners.html#troubleshooting) section might be a great place for this. 

