Snipet: [Create Git repositories with random data for testing](https://gitlab.com/-/snippets/2486970)

---

By default, `git branch` uses a pager (like the one you get with a command like `less` or `more`). This can be annoying in scripts like the one below -- but it's kind of nice when working interactively. 

To solve this in scripts, I do:

```
git branch | cat -n
```

That has the added bonus of telling me how many branches are returned. You can adjust this with:


```bash
git config --global pager.branch false
```

Kudos to [this SO answer](https://stackoverflow.com/a/48370253).

---


For a ticket, I assembled these commands to permit me to rapidly create a new MR. 

```
RANDOM_INFO=$(date +%A-%m-%B-%d_at_%H-%M-%S) &&  LOCAL_BRANCH=$RANDOM_INFO &&  echo $LOCAL_BRANCH && echo "remote branch"  &&    REMOTE_BRANCH=$RANDOM_INFO && echo $REMOTE_BRANCH &&   git checkout -b dev/whatever-$LOCAL_BRANCH
date >> package.json 
COMMIT_MESSAGE=$(curl --silent  https://whatthecommit.com/index.txt)
git commit -m"Meow: $COMMIT_MESSAGE" package.json
git push -u origin dev/whatever-${LOCAL_BRANCH}:dev/whatever-${REMOTE_BRANCH}   -o merge_request.create   -o merge_request.target=master   -o merge_request.merge_when_pipeline_succeeds  -o ci.variable="CUTE_MODE=enabled"  -o ci.variable="CUTE_CATS=YESPLEASE " 
```



  - 🔖 I have found a few different tickets where this is helpful. 
  - 🐈 I built [gitcute.cat](https://gitcute.cat) so I can get cuter commit messages from `https://gitcute.cat/commit` than what `whatthecommit.com` provides.


Looking at it line by line:

Whoo, OK. 

  - Create a variable called `RANDOM_INFO` for use in the local branch name. This uses an incantation of the `date` command to ensure there are no spaces or conflicts if the command is run later. (OK, within the same second, you'll have a conflict but...)
  - 
```
RANDOM_INFO=$(date +%A-%m-%B-%d_at_%H-%M-%S) &&  LOCAL_BRANCH=$RANDOM_INFO &&  echo $LOCAL_BRANCH && echo "remote branch"  &&    REMOTE_BRANCH=$RANDOM_INFO && echo $REMOTE_BRANCH &&   git checkout -b dev/whatever-$LOCAL_BRANCH
```

Next, we update `package.json`  with the output of  the `date` command. 

```
date >> package.json 
COMMIT_MESSAGE=$(curl --silent  https://whatthecommit.com/index.txt)
git commit -m"Meow: $COMMIT_MESSAGE" package.json
git push -u origin dev/whatever-${LOCAL_BRANCH}:dev/whatever-${REMOTE_BRANCH}   -o merge_request.create   -o merge_request.target=master   -o merge_request.merge_when_pipeline_succeeds  -o ci.variable="SANITY_MODE=TROY_SANITY_SHORT"  -o ci.variable="CUTE_CATS=YESPLEASE " 
```



## The Above...but Cuter

```
RANDOM_INFO=$(date +%A-%m-%B-%d_at_%H-%M-%S) &&  LOCAL_BRANCH=$RANDOM_INFO &&  echo $LOCAL_BRANCH && echo "remote branch"  &&    REMOTE_BRANCH=$RANDOM_INFO && echo $REMOTE_BRANCH &&   git checkout -b dev/whatever-$LOCAL_BRANCH
date >> README.md 
COMMIT_MESSAGE=$(curl --silent  https://gitcute.cat/commit)
git commit -m"🐈 Meow: $COMMIT_MESSAGE" README.md
git push -u origin dev/whatever-${LOCAL_BRANCH}:dev/whatever-${REMOTE_BRANCH}   -o merge_request.create   -o merge_request.target=main   -o merge_request.merge_when_pipeline_succeeds  -o ci.variable="CUTE_MODE=enabled"  -o ci.variable="CUTE_CATS=YESPLEASE " 
```
