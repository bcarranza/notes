

Renew certs manually:

```
sudo gitlab-ctl renew-le-certs
```


Or make your own certs: [[Self-Signed Certificates]] are fun!