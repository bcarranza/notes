
⚠️ EXERCISE CAUTION! ⚠️

```
# Get the current gitlab_repository_downloads_path
Gitlab.config.gitlab.repository_downloads_path

# Run the RepositoryArchiveCleanUpService manually
RepositoryArchiveCleanUpService.new(0).execute
```

A modified version of what's found in [this issue](https://gitlab.com/gitlab-org/gitlab/-/issues/299025).

## Object Storage Info

```
::Packages::PackageFileUploader.object_store_enabled?
::Packages::PackageFileUploader.options
```

## GitLab Configs

```
pp Gitlab.config.artifacts
pp Gitlab.config.object_store
```

## Artifacts

```
artifacts_deleted = 0
::Ci::JobArtifact.find_each do |artifact| ### Iterate artifacts
  artifact.destroy!   
end
puts "Count of identified/destroyed invalid references: #{artifacts_deleted}"
```