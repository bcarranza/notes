

Get a local version of `jsonvisio.com` [via Docker](https://github.com/AykutSarac/jsonvisio.com#-docker): 


```bash
git clone https://github.com/AykutSarac/jsonvisio.com.git
cd jsonvisio.com
podman build -t jsonvisio .
podman run -p 8888:8080 jsonvisio
```



