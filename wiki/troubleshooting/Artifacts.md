When thinking about artifacts, there are a few things to keep in mind. 

  - There are **job artifacts** in addition to **pipeline artifacts**. 
  - It's a bit less common to talk about **pipeline artifacts** but they should be taken in to account. 

## Pipeline Artifacts

Docs: [Pipeline Artifacts](https://docs.gitlab.com/ee/ci/pipelines/pipeline_artifacts.html)

These are not directly controlled by `.gitlab-ci.yml`! These are associated with test coverage visualization. 

> Pipeline artifacts are used by the [test coverage visualization feature](https://docs.gitlab.com/ee/ci/testing/test_coverage_visualization.html) to collect coverage information.


You can take a look at the `gitlab-org/ci-sample-projects/test-report` [sample project](https://gitlab.com/gitlab-org/ci-sample-projects/test-report) for an example. 

When looking at **Settings** > **Usage Quotas** in a project, you'll see a category for **Artifacts**. That includes the sum of job/build artifacts and pipeline artifacts. 




## API

With `?statistics=true`, you can use the get a single project API to get a bit more info about disk usage. Here's an example of that section of the output:


```json
	"statistics": {
		"commit_count": 2,
		"storage_size": 312093,
		"repository_size": 283115,
		"wiki_size": 0,
		"lfs_objects_size": 0,
		"job_artifacts_size": 28978,
		"pipeline_artifacts_size": 0,
		"packages_size": 0,
		"snippets_size": 0,
		"uploads_size": 0
	},
```

