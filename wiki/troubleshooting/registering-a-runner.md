```
# cat values.yaml
gitlabUrl: https://gitlab.carranza.engineer
runnerRegistrationToken: THETOKEN

runners:
  config: |
    [[runners]]
      [runners.kubernetes]
        image = "ubuntu:22.04"%
```


➜  24-friday 

```
# helm install --namespace runaway gitlab-runner -f values.yaml gitlab/gitlab-runner --version 0.49.3
Error: INSTALLATION FAILED: cannot re-use a name that is still in use
```

It does not like that the name `gitlab-runner` is already in use. Luckily: that can be anything:


➜  24-friday 

```
# helm install --namespace runaway cutegitlabrunner -f values.yaml gitlab/gitlab-runner --version 0.49.3
NAME: cutegitlabrunner
LAST DEPLOYED: Fri Feb 24 11:21:40 2023
NAMESPACE: runaway
STATUS: deployed
REVISION: 1
TEST SUITE: None
NOTES:
Your GitLab Runner should now be registered against the GitLab instance reachable at: "https://gitlab.carranza.engineer"
```

I made some more changes to my `values.yaml` and want to deploy them:



```
# helm install --namespace runaway cutegitlabrunner -f values.yaml gitlab/gitlab-runner --version 0.49.3
Error: INSTALLATION FAILED: cannot re-use a name that is still in use
```

We can still use `cutegitlabrunner`! The problem is that we said to install but we should have given the option to upgrade.

```
helm upgrade   --namespace runaway cutegitlabrunner -f values.yaml gitlab/gitlab-runner --version 0.49.3
Release "cutegitlabrunner" has been upgraded. Happy Helming!
NAME: cutegitlabrunner
LAST DEPLOYED: Fri Feb 24 11:23:03 2023
NAMESPACE: runaway
STATUS: deployed
REVISION: 2
TEST SUITE: None
NOTES:
Your GitLab Runner should now be registered against the GitLab instance reachable at: "https://gitlab.carranza.engineer"
```


...what do you know? Jobs are running!


The final `values.yaml` I used is:


```
gitlabUrl: https://gitlab.carranza.engineer
runnerRegistrationToken: THETOKEN

rbac:
  create: true

runners:
  config: |
    [[runners]]
      [runners.kubernetes]
        image = "ubuntu:22.04"
```

The `rbac.create` thing is handy if you encounter an error like this:

```
ERROR: Job failed (system failure): prepare environment: setting up credentials: secrets is forbidden: User "system:serviceaccount:runaway:default" cannot create resource "secrets" in API group "" in the namespace "runaway". Check https://docs.gitlab.com/runner/shells/index.html#shell-profile-loading for more information
```

Read about [enabling RBAC](https://docs.gitlab.com/runner/install/kubernetes.html#enabling-rbac-support) for more information. 


I also talk about this exact topic:

[[Runner on Kubernetes]]

[[CI + K8S]]