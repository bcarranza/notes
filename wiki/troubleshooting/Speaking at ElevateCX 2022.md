# Speaking at Elevate CX 2022

> On Thursday, September 29th, I gave a talk titled **Troubleshooting like Batman** at [ElevateCX](https://elevatecx.co) in beautiful Denver, Colorado. On this page, please find the notes on things I learned from that experience. I hope that these notes are helpful for colleagues who are interested in giving talks and in continuing to improve their ability to deliver a great customer experience. 


## Giving a Talk

### General 

  - Consider using art you create with DALL-E 2, Midjourney or similar in your slides. 
  - Consider a "throwaway" slide as your second slide. 
    - Get the audience curious while you situate yourself. 
    - My second slide was the one with the computer-generated art depicting a futuristic Batman troubleshooting computers. That gave me a few moments to talk about something lighthearted while I slowed down my breathing and oriented myself in the room. 
  - Remember to **breathe** and speak **slowly**. You should _probably_ speak more slowly than you think you should.
  - Record yourself giving your talk. 
	  - Watch the recording. 
	  - Take notes. 
	  - Improve based on the notes. 
	  - Repeat.

Please please please read Brendan O'Leary's [What Makes a Great Tech Talk](https://boleary.dev/blog/2022-09-21-what-makes-a-great-tech-talk.html) post for some ✨ absolutely ✨ amazing ✨ advice! It _really_ is worth the read: please! 🥹

At the end of the day: remember that you are a human being! 

### GitLab-specific  

  - Check out the Speakers Resources page. 
  - Consider joining a Lean Coffee. 
    - If you can't: take a look at the notes from the meetings for some good tips on improving your talk.

## CX

> The conference that I spoke at is all about CX (customer experience).

  
## Troubleshooting like Batman

> The talk that I gave focused on the similarities between how Batman solves problems and how we solve problems. 

**TODO** You can [watch the talk](). 
**TODO** You can watch a version of the talk I recorded in November 2022.
You can [download the slides (PDF)](Troubleshooting-like-Batman_Brie-Carranza_ElevateCX-2022_with-notes.pdf). 


![](elevate-listing.png)


### The Troubleshooting Toolbox

> I realized a bit too late that I could extend that Batman analogy a bit further and talk about Batman's utility belt. I like the alliteration of **Troubleshooting Toolbox**. 

The hard part is knowing when it's appropriate to use which tool. Each tool won't always be helpful. Each tool has a time. 

  - If the problem statement is not clear: use the **clarify the problem statement** approach instead of **reproduce the behavior**. 

These are the troubleshooting tools that I discussed in the talk along with some expanded thoughts on each:


#### Reproduce the behavior

Can you reproduce the undesired or unexpected behavior in the problem statement?

  - The process of attempting to reproduce the behavior is likely to generate questions. As you are standing up a system to reproduce, be mindful of relevant settings. Do you know what value the customer has? You might not have sufficient info to attempt to reproduce. 

OK, let's say you do have enough info to work towards reproducing the behavior. 

Whether you can reproduce the behavior always, sometimes or never: there is something to be learned or gained from that. 

##### I can always reproduce.
##### I can sometimes reproduce. 

That sounds _intermittent_: fun! But seriously:

##### I can never reproduce. 

Interesting. There is probably some additional information about the customer's environment that you do not have. 

#### Reduce complexity

#### (Re)set expectations

#### Clarify the problem statement

> If you cannot clearly articulate the problem, how can start solving it? How will you know you finished solving it?



#### Increase verbosity

  - Get more information from the system that you are troubleshooting -- and from related systems that process the request. 


##### Example

  - **Problem**: The username and password being passed for HTTP authentication are not being used. 

After increasing verbosity, we see that the system receives a username/password pair **and** HTTP headers for authentication. 

Increasing verbosity gave us this additional information and permitted us to learn more about the system. It turns out that this system uses HTTP headers if it gets both headers and a username/password pair. 

With that information, the solution becomes clear. 

  - **Solution**: Stop passing the HTTP headers and _only_ pass the username/password pair.

#### Ask questions
 
