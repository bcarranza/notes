
```
cut -d" " -f3- /var/log/gitlab/sidekiq/current | jless
```


What time period do these logs cover?:

```
# cut -d" " -f3- current | jq -r '.time' | sort -n | sed -n '1p;$p'
2025-01-23T10:48:14.607Z
2025-01-23T17:12:06.008Z
```

## Class and the Source Code

Regardless of how you get the `sidekiq` logs, you'll notice that most entries contain a `class`.

To watch these live:

```
tail -F current  | jq '.class'
```


- See [app/workers/all_queues.yml](https://gitlab.com/gitlab-org/gitlab/-/blob/master/app/workers/all_queues.yml) and [ee/app/workers/all_queues.yml](https://gitlab.com/gitlab-org/gitlab/-/blob/master/ee/app/workers/all_queues.yml).
- [Sidekiq development guidelines](https://docs.gitlab.com/ee/development/sidekiq/)

### Jobs

🤭 These are jobs that run in `sidekiq` and not on the Runner. 
