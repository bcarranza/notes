DAST - Dynamic Application Security Testing is a super cool feature!

Here's a sample working project:

  - [bcarranza/dast](https://gitlab.com/bcarranza/dast)

A collection of examples that are useful for [[Testing authentication and DAST]].

## Is authentication working? Can DAST authenticate to my site?

The [authentication debug output report]() is your BFF. It includes screenshots of what the browser saw when attempting to log in to your site!

However: the general guidance is to avoid dealing with authentication when scanning via DAST. 

Consider that it is **not** recommended to perform DAST scans (especially authenticated scans) against production. 

That means that the application should be deployed in a pre-production environment so that authenticated scans can be run against it. The pre-production environment should be configured so that authenticated DAST scans won't have adverse impacts on other elements of your infrastructure. You may wish to deploy the pre-production version of the app via GitLab CI. 

However, since the app is being deployed in a pre-production environment, it is generally recommended to bypass or disable authentication **for DAST in the application's pre-production environment**. 

Why?

 - Authentication via DAST is hard.
	 - The OWASP ZAP project says "make your life easier" by bypassing or simplifying authentication. 
 - There may not be much to be gained by having DAST authenticate and **then** crawl for sites to scan. 


# SPA Day

Let's experiment with SPAs: [[Single Page Applications]] and DAST.

# 🔖 Bookmarks

https://gitlab.com/gitlab-org/security-products/dast-runner-validation/-/blob/main/src/validate.sh 

