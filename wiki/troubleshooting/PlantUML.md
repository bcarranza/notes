

- Docs: [PlantUML](https://docs.gitlab.com/ee/administration/integration/plantuml.html)
- https://gitlab.labranet.jamk.fi/tools-and-services/mkdocs-plantuml-docker



```bash
docker run -d --name plantuml -p 8080:8080 plantuml/plantuml-server:tomcat
```