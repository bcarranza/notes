# LDAP

> Lightweight Directory Access Protocol



```shell
/opt/gitlab/embedded/bin/openssl s_client -connect ldap.jumpcloud.com:636 -servername ldap.jumpcloud.com -showcerts
```


See an [[showcerts|example of the expected output]].
