
Use the query below to count the number of **High** vulnerabilities per day in a group. 

```
{
  group(fullPath: "gitlab-gold/briecarranza")
  {
    vulnerabilities {
      nodes {
        project {
          vulnerabilitiesCountByDay(startDate: "2022-10-26", endDate: "2022-10-26") {
            nodes {
              high
            }
          }
        }
      }
    }
  }
}
```


Use the mutation below to _remove_ or unset a complicance framework from a project:

```
mutation {
  projectSetComplianceFramework(input: {projectId: "gid://gitlab/Project/1234567", complianceFrameworkId: null}) {
    errors
  }
}
```

A [few additional examples](https://docs.gitlab.com/ee/api/graphql/index.html#view-graphql-examples) are mentioned in the docs. 


```graphql
query getMRSecurityReport {
  project(fullPath: "gl-demo-ultimate-bcarranza/no-scan-target-branch") {
    mergeRequest(iid: "1") {
      title
      hasSecurityReports
      findingReportsComparer(reportType: SAST) {
        status
        report {
          headReportCreatedAt
          baseReportCreatedAt
          baseReportOutOfDate
          added {
            uuid
            title
            description
            severity
          }
          fixed {
            uuid
            title
            description
            severity
          }
        }
      }
    }
  }
}
```

- https://about.gitlab.com/blog/2022/02/02/graphql-vulnerability-api/
- 


## 📚 References

  - [GraphQL reference](https://docs.gitlab.com/ee/api/graphql/reference/)
  - 