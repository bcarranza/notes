

## Artificially Reproduce "Stuck" Pending Background Migrations

	  - https://gitlab.com/gitlab-org/gitlab/-/issues/356875


## Inspecting Them

```
gitlab-rails runner -e production 'puts Gitlab::Database::BackgroundMigrationJob.pending[0].pretty_inspect'
```