
LDAP : form-based | we can ask questions
SAML: browser/client based | we only get info when the user goes through the authentication process


If you get to `OmniauthCallbacksController` the user has successfully authenticated. Maybe they can't use GitLab yet (they'll either get connected to ane xisting account, a new account will be created or their access will be denied)


We favor looking up username via LDAP (we can learn more) vs SAML (just the attributes). We generate that info from LDAP or straight from the auth hash.

The info is sent to the user create service.


`o_auth/user.rb`

### The Auth Hash

The structure is a bit opaque.

One of the most common failures seen is SAML not sending an `email` attribute. This is a good place to check. We might now have some better error messaging around that. 


- [[Authentication]]
- [[LDAP]]
	- [[ldap-gitlab-jumpcloud]]
	- [[GitLab Helm Chart + JumpCloud LDAP]]