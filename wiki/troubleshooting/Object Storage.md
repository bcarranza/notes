
```ruby
Ci::PipelineArtifact.all.map { |artifact| [artifact.project.full_path, artifact.pipeline_id, artifact.file.path] }
```

## When there are _some_ uploads


```shell
# sudo gitlab-rake gitlab:uploads:check

Checking integrity of Uploads
- 1..3: Failures: 0
Done!
```


## When there are no uploads

```shell
sudo gitlab-rake gitlab:uploads:check
```


```shell
root@sr-env-ae706f19-omnibus:~# sudo gitlab-rake gitlab:uploads:check
Checking integrity of Uploads
Done!
root@sr-env-ae706f19-omnibus:~#
```

Let's increase the verbosity a bit.

```shell
sudo gitlab-rake gitlab:uploads:check VERBOSE=1
```