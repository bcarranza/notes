See [RFC 7644](https://www.rfc-editor.org/rfc/rfc7644.html) for SCIM:

# System for Cross-domain Identity Management

We have a docs page called [SCIM provisioning using SAML SSO for GitLab.com groups](https://docs.gitlab.com/ee/user/group/saml_sso/scim_setup.html).

  - SAML: Security Assertion Markup Language
  - ACS: Assertion Consumer Service (usually in the context of the ACS URL)

## OK, so....SCIM vs SAML?

From a GitLab perspective....

[SCIM](https://docs.gitlab.com/ee/user/group/saml_sso/scim_setup.html):

> System for Cross-domain Identity Management (SCIM), is an open standard that enables the automation of user provisioning. When SCIM is provisioned for a GitLab group, membership of that group is synchronized between GitLab and the identity provider.


[SAML](https://docs.gitlab.com/ee/user/group/saml_sso/index.html)  (for GitLab.com):

> SAML on GitLab.com allows users to sign in through their SAML identity provider. If the user is not already a member, the sign-in process automatically adds the user to the appropriate group.

[SAML OmniAuth Provider](https://docs.gitlab.com/ee/integration/saml.html):

> GitLab can be configured to act as a SAML 2.0 Service Provider (SP). This allows GitLab to consume assertions from a SAML 2.0 Identity Provider (IdP), such as Okta to authenticate users.


## Docs

  - [Table comparing SaaS and Self-Managed](https://docs.gitlab.com/ee/administration/auth/index.html#saas-vs-self-managed-comparison) with regard to external **authentication** and **authorization** providers. 
  - [SCIM API](https://docs.gitlab.com/ee/api/scim.html) (used by the SCIM provider integration AKA not for humans)

## Issues

  - https://gitlab.com/gitlab-org/gitlab/-/issues/363058


## Rails Console

You can access an `Identity`. 

```
Identity.where(extern_uid: 'old-id').update!(extern_uid: 'new-id')`
```


[source](https://docs.gitlab.com/ee/administration/auth/index.html#change-apps-or-configuration)





---


## Permit multiple email addresses during user provisioning via SCIM


I created the [Permit multiple email addresses during user provisioning via SCIM](https://gitlab.com/gitlab-org/gitlab/-/issues/371405) issue proposing:

> Adjust the way GitLab handle SCIM provisioning to consume _all_ email addresses associated with a user.
> 
I believe that there are two cases to cover:
> 
In addition to adding the primary email address with a `type` of `work`, also add:
> 
> - Multiple emails where the `type` is `work` (and only one if the primary)
> - Multiple emails where the `type` is **not** `work`



A colleague [wrote](https://gitlab.com/gitlab-org/gitlab/-/issues/371405#note_1073064306):


> Confirmed. We currently only use the first email we find that is a `work` type email or `primary` email - [https://gitlab.com/gitlab-org/gitlab/-/blob/b9ab3f1773a3e7ee4dd5ca8a4dfd8aebb810d13e/ee/lib/ee/gitlab/scim/params\_parser.rb#L66](https://gitlab.com/gitlab-org/gitlab/-/blob/b9ab3f1773a3e7ee4dd5ca8a4dfd8aebb810d13e/ee/lib/ee/gitlab/scim/params_parser.rb#L66).


We were definitely on the right track when looking into this `params_parser.rb` does the lifting. 

[the linked line](https://gitlab.com/gitlab-org/gitlab/-/blob/b9ab3f1773a3e7ee4dd5ca8a4dfd8aebb810d13e/ee/lib/ee/gitlab/scim/params_parser.rb#L66)


```ruby
`email = emails.find { |email| email[:type] == 'work' || email[:primary] }`
```


---


Authentication: who is knocking, please?
Authorization: does the person who is knocking have access to the door they are knocking on?