
Docs: [LDAP](https://docs.gitlab.com/charts/charts/globals.html#ldap)

You'll need a `secret` and `values.yaml` content **like** this:

```yaml
ldap:
  preventSignin: false
  servers:
    # 'main' is the GitLab 'provider ID' of this LDAP server
    main:
      label: '🚀 LDAP'
      host: 'ldap.jumpcloud.com'
      port: 389
      uid: 'uid'
      bind_dn: 'uid=tanuki,ou=Users,o=abcdefghijklmno,dc=jumpcloud,dc=com'
      base: 'o=abcdefghijklmno,dc=jumpcloud,dc=como=abcdefghijklmno,dc=jumpcloud,dc=com'
      password:
        secret: my-ldap-password-secret
        key: the-key-containing-the-password

```



## Related Pages

- [[LDAP]]
- [[Authentication]]
- [[ldap-gitlab-jumpcloud]]


