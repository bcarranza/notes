# Mattermost

> What matters the most?


  - [Upgrading Mattermost](https://digital-thunder.gitlab.io/notepad/post/2021-06-mattermost-install-upgrade/)
  - Snippet: [get Mattermost version](https://gitlab.com/bcarranza/notebook/-/snippets/2046593)
## Quickstart: Set up GitLab Mattermost

From a fresh install of GitLab, here's what I did:

I wanted to run GitLab Mattermost at "https://mattermost.secretbank.org". I created an **A** record that pointed to the IP of my GitLab instance.

Per the [getting started](https://docs.gitlab.com/omnibus/gitlab-mattermost/) docs, I then added this line to `gitlab.rb`:

```
mattermost_external_url 'https://mattermost.secretbank.org'
```

Now, `gitlab-ctl reconfigure`

  - Browse to `https://mattermost.secretbank.org`
  - Authorize GitLab to act as your SSO provider for GitLab Mattermost
  
## What version of GitLab Mattermost do I have?

Use this [get-gitlab-mattermost-version.sh](https://gitlab.com/bcarranza/notebook/-/snippets/2046593) snippet. 


The snippet (presently) amounts to:

```
cd /opt/gitlab/embedded/service/mattermost/i18n
# root@bcarranza-omnibus-delta:/opt/gitlab/embedded/service/mattermost/i18n# 
/opt/gitlab/embedded/bin/chpst -e /opt/gitlab/etc/mattermost/env -P -U mattermost:mattermost -u mattermost:mattermost /opt/gitlab/embedded/bin/mattermost --config=/var/opt/gitlab/mattermost/config.json version
```

