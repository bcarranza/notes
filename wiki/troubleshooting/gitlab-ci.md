
# Troubleshooting GitLab CI

[[GitLab Runner]]

## Sample Jobs


##### A job that fails at random

There are a bunch of ways to do this. Pop the following into `fail.sh` or similar:


```
EXIT_CODE=$( curl --silent "http://www.randomnumberapi.com/api/v1.0/random?min=0&max=1&count=1" | cut -c2)
echo $EXIT_CODE
exit $EXIT_CODE
```


##### A job that tells you about your `git` clone/fetch results

```yaml
git info check:
  script:
    - git describe --tags --always --abbrev=0
    - if test -f ".git/shallow" ; then cat .git/shallow ; else echo "nope"; fi
    - git tag --list
    - git remote -v
    - git ls-remote --tags origin
    - echo $GIT_STRATEGY
    - echo $GIT_DEPTH
    - git fetch --tags
    - git describe --tags --always --abbrev=0
    - git tag --list
```


## Sample Projects

These are the projects I use frequently for testing miscellaneous GitLab CI things. 
  - [GitLab Smoke Tests](https://gitlab.com/gitlab-com/support/toolbox/gitlab-smoke-tests): this is amazing and tests job artifacts and caching as well as pushing to Container Registry and Package Registry (Maven, NPM, NuGet, Helm). 
  - [detecting-secrets](https://gitlab.com/bcarranza/detecting-secrets): A small project I put together to ensure that [Secret Detection](https://docs.gitlab.com/ee/user/application_security/secret_detection/) works as expected. 
  - https://gitlab.com/gitlab-ci-utils/gitlab-ci-templates
  - https://docs.gitlab.com/ee/user/application_security/dast_api/#example-dast-api-scanning-configurations
  - https://gitlab.com/gl-release/blue-green-example

### Templates to Include

These are a few places with very interesting GitLab CI templates that can be included:

  - [to be continuous](https://to-be-continuous.gitlab.io/doc/)
	  - Try the [generator](https://to-be-continuous.gitlab.io/kicker/)! 
  - [r2devops](https://r2devops.io)

### Testing Terraform-related Stuff

  - [TerraGoat](https://github.com/bridgecrewio/terragoat) - TerraGoat is Bridgecrew's "Vulnerable by Design" Terraform repository.
  - [tf-performance-issue-test](https://github.com/eriksw/tf-performance-issue-test/)
  - [terraform-examples](https://github.com/futurice/terraform-examples)  and a [few more examples](https://github.com/ksatirli/terraform-github-repository/tree/main/examples)
  - You didn't ask but the [top 10 most common mistakes wwith Terraform](https://blog.pipetail.io/posts/2020-10-29-most-common-mistakes-terraform/) is worth a quick review. 
  - Try [Reverse Terraform](https://github.com/GoogleCloudPlatform/terraformer). 

#### Generating `.tf`

  - brainboard - drag-and-drop infrastructure builder, save the generated `*.tf`
  - cloudcraft
  - https://www.reddit.com/r/devops/comments/ezbbus/automatically_generate_terraform_code_by_scanning/
  - [tfvar](https://github.com/shihanng/tfvar)
  - [Terraform Generator by Digger](https://www.producthunt.com/products/terraform-generator-by-digger) seems cool but wants to sign in with AWS. That's a trend with these tools. We see that [Cycloid](https://www.cycloid.io/pricing) is _free_ when you connect your cloud account. 
  - https://marketplace.visualstudio.com/items?itemName=mindginative.terraform-snippets

## Dynamically Generated Child Pipelines

### Docs

### External Examples and Write-Ups
  - [node-gitlab-ci](https://github.com/devowlio/node-gitlab-ci) -- Dynamically create your `.gitlab-ci.yml` from TypeScript .ts files! 
  - [A new era for GitLab CI: Dynamic Child Pipelines](https://www.objectif-libre.com/en/blog/2021/02/23/a-new-era-for-gitlab-ci-dynamic-child-pipelines/)

## Troubleshooting

### Problem: `remote: You are not allowed to download code from this project.`

```
remote: You are not allowed to download code from this project.
```

https://forum.gitlab.com/t/runner-cant-pull-code-from-server/14678

**Solution**: Add yourself as a member of the project. 


---

## Kerberos

> GitLab CI/CD doesn’t work with a Kerberos-enabled GitLab instance unless the integration is [set to use a dedicated port](https://docs.gitlab.com/ee/integration/kerberos.html#http-git-access-with-kerberos-token-passwordless-authentication).


The change required to get GitLab CI working in Kerberized environment is [documented](https://docs.gitlab.com/ee/integration/kerberos.html#http-git-access-with-kerberos-token-passwordless-authentication) and amounts to updates to `gitlab.rb` and a `gitlab-ctl reconfigure`.  Additionally: Git remote URLs need to be updated to use that port for Kerberos ticket-based auth to work. 


Example: `https://gitlab.example.com:8443/whatever/cutecats.git`


