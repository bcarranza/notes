When troubleshooting, it's often helpful to have test sites that provide something closer to "known good" than the system you are working on. I want to start collecting useful test sites/services/apps et cetera. 

⚠️ As always, make sure that you are working within the terms of the test site. I present these with no guarantee.

  - [Authentication Test](https://authenticationtest.com)
  - [mock.codes](https://mock.codes/) - Mocked HTTP Status codes
	  - mock.codes provides for an easy way to reference and test HTTP responses.
  - [httpbin](https://httpbin.org/)
	  - It looks like the folks at [k6](https://k6.io/) have a [test `httpbin` instance](https://httpbin.test.k6.io/)...and `httpie` has [one, too](https://pie.dev/). 🥧


The above options are awesome but I want something that I:

  - control
  - can (readily) deploy within a GitLab CI pipeline.

_WIP_ The available options to consider include:

  - [x] Self-hosted `httpbin` (`docker run -p 80:80 kennethreitz/httpbin`)
  - [ ] [webfakes](https://cran.r-project.org/web/packages/webfakes/index.html)



## Container Registry


```sql
SELECT created_at,migration_state FROM container_repositories WHERE project_id=14;
```


### Using a Bearer token to authenticate to  HTTPbin

Using `curl`:

```
curl --request GET \
  --url https://httpbin.org/bearer \
  --header 'Authorization: Bearer cats'
{
  "authenticated": true,
  "token": "cats"
}
```


...

or `httpie`:

```
https -A bearer -a token https://httpbin.org/bearer
HTTP/1.1 200 OK
Access-Control-Allow-Credentials: true
Access-Control-Allow-Origin: *
Connection: keep-alive
Content-Length: 49
Content-Type: application/json
Date: Thu, 10 Nov 2022 04:18:23 GMT
Server: gunicorn/19.9.0

{
    "authenticated": true,
    "token": "token"
}
```

  - See [[Testing Vulnerable Apps]]. 

For [[DAST]] and authentication:

  - Don't. 
	  - Deploy the app in an isolated pre-production environment. There you should bypass, disable or greatly simplify authentication.
		  - See [Authentication - Make your Life Easier](https://www.zaproxy.org/docs/authentication/make-your-life-easier/) directly from `zaproxy.org` for a bit more. 
  - Sites like [Authentication Test](https://authenticationtest.com/) are great. The linked `authenticationtest.com` has everything from super simple form auth to interactive MFA. 
  - I also like `https://httpbin.org`, which can easily be deployed for self-hosted usage. 


## DAST and HTTP Headers


To do [basic authentication with DAST](https://gitlab.com/gitlab-org/gitlab/-/issues/3703050), do something like:

```
dast:
    variables: DAST_REQUEST_HEADERS: "Authorization: Basic $BASE64_AUTH"
```

Make `$BASE64_AUTH` a masked CI variable.

To do `Bearer` auth, do something like:

## Authenticating to a GitLab instance with DAST

You can use DAST to authenticate to and then scan a self-managed GitLab instance. Here's how I'm **successfully** doing that as of November 2022:




```
include:
  - template: DAST.gitlab-ci.yml

stages:
  - 🍓🥝🍏
  - 🍋🍯🌻
  - 🐈💖🐕
  - 🦋🦄​🌈

scan gitlab.example.com:
  extends: dast
  stage: 🍓🥝🍏
  variables:
    DAST_AUTH_REPORT: "true"
    DAST_PATHS: "/admin,/-/graphql-explorer"
    DAST_WEBSITE: "https://gitlab.example.com/"
    DAST_AUTH_URL: "https://gitlab.example.com/users/sign_in#login-pane" # LDAP is enabled but this lets me skip using LDAP for the DAST scan.
    DAST_HTML_REPORT: "lol.txt"
    DAST_USERNAME: "root"
    DAST_PASSWORD: $DAST_OMG_PASSWORD
    DAST_USERNAME_FIELD: "id:user_login"
    DAST_PASSWORD_FIELD: "id:user_password"
    DAST_SUBMIT_FIELD: "name:button"
  artifacts:
    paths: [gl-dast-debug-auth-report.html]
    when: always
```



---


# Useful Commands

> These don't have a dedicated page but they belong _somewhere_.


```bash
date +%Y-%m-%B-%d--%T-$(openssl rand -hex 20)
```


Let's use a slightly modified version to quickly create a unique tag:

```bash
git tag $(date +%Y-%m-%B-%d--%H%M%S-$(openssl rand -hex 20))
```


See [[Automating select git operations]].
