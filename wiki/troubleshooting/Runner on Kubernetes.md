# November 2023

## Using runner aliases

```
[[runners.kubernetes.host_aliases]]
  ip = "172.65.251.78"
  hostnames = ["gitlab.com"]
```

The [docs](https://docs.gitlab.com/runner/executors/kubernetes.html) say: 

> List of additional host name aliases that will be added to all containers. [Read more about using extra host aliases](https://docs.gitlab.com/runner/executors/kubernetes.html#add-extra-host-aliases).

See [additional examples](https://docs.gitlab.com/runner/executors/kubernetes.html#add-extra-host-aliases). 

Read about the security implications of [hardcoding IP addresses](https://knowledge-base.offensive360.com/HardcodedIpAddress/). 

# September 2023

## Troubleshooting a registered Runner

Let's say you have a `values.yaml` file for a Runner with a bunch of comments. Here are a few ways to see everything _except_ the comments:

- jless
- `yq '... comments=""'  runner-values.yaml`
	- [Remove (strip) all comments](https://mikefarah.gitbook.io/yq/operators/comment-operators#remove-strip-all-comments)


Looking through kubeSOS output to see if the Runner was able to check in:

```
rg '/jobs/request' | cut -d" " -f2- | jq -r '[."user_agent",.remote_addr,.status] | join (",")'
```


# April 2023

## Registering a Runner 

```
kubectl create namespace ops-carranza-engineer
helm install --namespace ops-carranza-engineer -f k8s-runner.yaml gitlab-runner gitlab/gitlab-runner --version 0.41.1
```


Let's say you've done this a billion times and need a unique name:

```bash
helm install --namespace ops-carranza-engineer -f ops-carranza-engineer.yaml march10 gitlab/gitlab-runner
```


# January 2023

It's _always_ DNS.  

If the DNS record for the coordinator URL


On `NOERROR` vs `NXDOMAIN`: they are different but you might get one when you expect the other. We observed a DNS server returning `NOERROR` and also not providing an answer. This is normal for when a wildcard record exists, we learned. 

```
dig @1.1.1.1 test.demo.example.com -t aaaa

; <<>> DiG 9.10.6 <<>> @1.1.1.1 test.demo.example.com -t aaaa
; (1 server found)
;; global options: +cmd
;; Got answer:
;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 9654
;; flags: qr rd ra ad; QUERY: 1, ANSWER: 0, AUTHORITY: 1, ADDITIONAL: 1

;; OPT PSEUDOSECTION:
; EDNS: version: 0, flags:; udp: 1232
;; QUESTION SECTION:
;test.demo.example.com. IN AAAA

;; AUTHORITY SECTION:
example.com.	86352	IN	SOA	ns1.mail.example.com. hostmaster.mail.example.com. 2023010310 7200 3600 1209600 86400

;; Query time: 103 msec
;; SERVER: 1.1.1.1#53(1.1.1.1)
;; WHEN: Tue Jan 03 10:55:29 EST 2023
;; MSG SIZE  rcvd: 134
```





## Recommended Reading

  - https://bluecatnetworks.com/blog/the-top-four-dns-response-codes-and-what-they-mean/
  - [[registering-a-runner]]


### Viewing

  - A [cat explains DNS](https://www.youtube.com/watch?v=4ZtFk2dtqv0)
