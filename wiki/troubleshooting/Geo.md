```
status = GeoNodeStatus.current_node_status
Geo::NodeStatusRequestService.new(status).execute
```


```
module Gitlab::Geo::LogHelpers
  def geo_logger; Gitlab::Geo::Logger.new(STDOUT); end
end
```


```
module Gitlab::Geo::LogHelpers
  def geo_logger; Gitlab::Geo::Logger.new(STDOUT); end
end
```



## Log Analysis


Looking for specific errors associatd with syncing `repository` objects:


```
egrep "(mismatch|checksummable|undefined)" geo.log | jq '. | select(all(.type; . == "repository")) | .message,.project_path'
```


For everything that is _not_ a repository:

```
egrep "(mismatch|checksummable|undefined)" geo.log | jq '. | select(all(.type; . != "repository")) | .message,.project_path'
```


Change the `.message,.project_path` to extract the bits that you care about. 