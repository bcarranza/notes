

  - [awesome-elasticsearch](https://github.com/dzharii/awesome-elasticsearch)


## 🔖 Bookmarks

  - [Advanced Search Rake tasks](https://docs.gitlab.com/ee/integration/advanced_search/elasticsearch.html#gitlab-advanced-search-rake-tasks)
  - [Product Direction - Global Search](https://about.gitlab.com/direction/global-search/)
  - [@bigcodesearch](https://twitter.com/bigcodesearch) on Twitter.




---

Get the list of pending migrations and the contents of `elasticsearch.log`. Address any errors so that the list of pending Elasticsearch migrations goes to `0`. What you want to see is this:

```
sudo gitlab-rake gitlab:elastic:list_pending_migrations

There are no pending migrations.
```


## Recreating the Index

Recreating the index is the option of last resort. While you want to take other less drastic steps first, eventually you'll get to a point where recreating the index is the next best step. 

It's the option of last resort in part because it takes so long to complete. This is where [zero downtime reindexing is nice](https://docs.gitlab.com/ee/integration/advanced_search/elasticsearch.html#zero-downtime-reindexing): the old index continues to be used while the new one is created. 

Zero downtime reindexing is _different_ than the normal reindexing method. The "normal reindexing" method is running the  `gitlab-rake gitlab:elastic:index` command. 

### Elasticsearch and Opensearch

GitLab introduced support for Opensearch [with 15.0](https://about.gitlab.com/releases/2022/05/22/gitlab-15-0-released/#advanced-search-is-compatible-with-opensearch) (May 2022). Prior to that, we only supported Elasticsearch.

  - [Introducing OpenSearch](https://aws.amazon.com/blogs/opensource/introducing-opensearch/) from the AWS blog 


---


