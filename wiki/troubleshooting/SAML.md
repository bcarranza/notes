hich # SAML: Testing and Troubleshooting

## Troubleshooting

  - [How to view a SAML response in your browser for troubleshooting](https://docs.aws.amazon.com/IAM/latest/UserGuide/troubleshoot_saml_view-saml-response.html)
	  - This is one of the single most helpful things for troubleshooting all kinds of SAML tickets.
	  - You can take a look at some [example SAML responses](https://www.samltool.com/generic_sso_res.php).

## Configuration

### Using JumpCloud as an IdP in GitLab

These docs links will help:

  - https://support.jumpcloud.com/support/s/article/saml-sso-troubleshooting-2019-08-21-10-36-47
  - https://support.jumpcloud.com/support/s/article/saml-configuration-notes-2019-08-21-10-36-47
  - https://support.jumpcloud.com/support/s/article/connecting-applications-with-jumpcloud-using-pre-built-connectors-2019-08-21-10-36-47
  - https://docs.gitlab.com/ee/integration/saml.html#configure-saml-support-in-gitlab
  - https://workos.com/docs/integrations/jumpcloud-saml/what-workos-provides

I have this up and running: no configuration of assertions required.

### Generating the SHA1 Fingerprint

> Run `openssl x509 -in <your_certificate.crt> -noout -fingerprint` to generate the SHA1 fingerprint that can be used in the `idp_cert_fingerprint` setting.

That will be different than `sha1sum your_certificate.crt`. You could use OneLogin's [Calculate Fingerprint](https://www.samltool.com/fingerprint.php) tool. 

### Settings in 💎 `gitlab.rb`

On [attributes](https://learn.microsoft.com/en-us/entra/identity-platform/saml-claims-customization#attributes), Microsofts suggests that the end user select the desired source for the `nameID` claim. This can be `Email` or something else. Let's focus in on `Email`. 

A few I'd like to focus in on:

- `name_identifier_format`
- `attribute_statements`
- `uid_attribute`

#### attribute_statements

The docs on configuring assertions say:

 See [`attribute_statements`](https://docs.gitlab.com/ee/integration/saml.html#map-saml-response-attribute-names) for:

- Custom assertion configuration examples.
- How to configure custom username attributes.

We also link straight to [saml.rb](https://github.com/omniauth/omniauth-saml/blob/master/lib/omniauth/strategies/saml.rb).
#### name_identifier_format

- [Microsoft identity platform uses Persistent as the nameID format.](https://learn.microsoft.com/en-us/entra/identity-platform/saml-claims-customization)
	- Source: [Customize SAML token claims](https://learn.microsoft.com/en-us/entra/identity-platform/saml-claims-customization)

The value in `gitlab.rb` should match what is sent by the IdP.

In the **SAML Response** from JumpCloud, I see: 


```
urn:oasis:names:tc:SAML:1.1:nameid-format:emailAddress
```

In `gitlab.rb` I have:

```
      name_identifier_format: "urn:oasis:names:tc:SAML:1.1:nameid-format:emailAddress"
```

When this is mismatched: nothing too bad seems to happen. With this obviously incorrect value, I was able to log in for the first time as a particular user: 

```
      name_identifier_format: "urn:oasis:names:tc:SAML:1.1:nameid-format:catmail"
```


[SAML 2.0 name identifier formats](https://www.ibm.com/docs/en/sva/9.0.1?topic=federations-saml-20-name-identifier-formats)

#### uid_attribute

Given an error like `Saml response missing 'uid' attribute`, what exactly is GitLab looking for and where?

- Let's inspect a SAML response for an environment where we know SAML is working.
- In the `omniauth` source, it looks like that error is triggered around [line 94](https://github.com/omniauth/omniauth-saml/blob/master/lib/omniauth/strategies/saml.rb#L94) of `lib/omniauth/strategies/saml.rb`.

That is if _whatever_ value that's being used for `uid_attribute` is missing. It _should_ be possible to [parse](https://github.com/omniauth/omniauth-saml/blob/master/lib/omniauth/strategies/saml.rb#L132) a raw SAML response the way that Omniauth does. We also [see](https://github.com/omniauth/omniauth-saml/blob/master/lib/omniauth/strategies/saml.rb#L142) that `saml_uid` is set as `@name_id`. I can totally reproduce this problem when I set `uid_attribute: "cutie"`.

- 🐾 The customer 100% needs to ensure that the SAML response from the IdP includes the value that GitLab expects as `uid_attribute`. This comes from the SAML response and should be the `name_id`. [source](https://docs.gitlab.com/ee/integration/saml.html#designate-a-unique-attribute-for-the-uid)
- 📋Check the response for JumpCloud to confirm it exists there.
- Look through [these lines](https://github.com/omniauth/omniauth-saml/blob/master/lib/omniauth/strategies/saml.rb#L90-L100).

## 🧪 Testing

🐳 All roads point to `kristophjunge/test-saml-idp`. The Apereo Foundation maintains `apereo/saml2-sp`. Read more in [Testing SAML2 Identity Providers](https://fawnoos.com/2023/08/18/saml2-idp-testing-sp-docker/) from August 2023.

- ⭐️ [Use SSO authentication locally](https://docs.akeneo.com/3.0/use_sso_authentication/index.html)
- [Public IDP](https://www.ssocircle.com/en/portfolio/publicidp/) from SSOCircle

## 💎 From the Source (Code)

What causes the button with the customized SAML label to appear? We know that we're looking for what controls whether the "or sign in with" text appears. 

After cloning the `gitlab-org/gitlab` source I used this `ripgrep` command to exclude specs and translations:

```
rg  -g '!{locale/*,spec/*}'  'or sign in with'
```

That led me to `app/views/devise/shared/_omniauth_box.html.haml`.

```
if any_form_based_providers_enabled? || password_authentication_enabled_for_web?
```

OK, so the "or sign in with" thing should appear if any form-based providers are enabled **or** if password authentication is enabled for the web.


## 🚀 ...and beyond! 

This kind of thing tends to be out of scope but it interesting nonetheless:

- [Claim transformations](https://learn.microsoft.com/en-us/entra/identity-platform/saml-claims-customization#claim-transformations)
- [Configuring NameId](https://www.identityserver.com/documentation/saml2p/config-idp/configuring-nameId/)