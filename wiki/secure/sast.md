# SAST

## Advanced SAST

- March 2024 | [Oxeye joins GitLab to advance application security capabilities](https://about.gitlab.com/blog/2024/03/20/oxeye-joins-gitlab-to-advance-application-security-capabilities/)
- September 2024 | [GitLab Advanced SAST is now generally available](https://about.gitlab.com/blog/2024/09/19/gitlab-advanced-sast-is-now-generally-available/)


### 🔖 Bookmarks

- [Feedback issue - GitLab Advanced SAST](https://gitlab.com/gitlab-org/gitlab/-/issues/466322)
- [Advanced SAST Demo](https://gitlab.com/gitlab-da/use-cases/advanced-sast-demo)