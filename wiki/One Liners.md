# One Liners

> I need a oneliner that....

```
 jq '."grpc.meta.client_name"' var/log/gitlab/gitaly/current | sort | uniq -c | sort -n
```

## GitLab Configs

Remove all uncommented lines from `gitlab.rb` -- with the version of `sed` provided on macOS. (Yes: `gsed` is available!)

```
grep -v '^#' gitlab.rb  | sed '/^[t|s]*$/d'
```

## What version of `x` is in version `y` of GitLab?

```shell
docker run -it gitlab/gitlab-ee:16.10.3-ee.0 patronictl version
```

```
docker exec -it gitlab grep -i gitaly /opt/gitlab/version-manifest.txt
```

## Tail but only some stuff

You can say `gitlab-ctl tail gitlab-rails nginx` to view **just** the logs from those services. 

---

Docs: [Parsing GitLab logs with `jq`](https://docs.gitlab.com/ee/administration/logs/log_parsing.html)

## Removes blank lines -- on Mac OS

```
sed -i.bak '/^[[:space:]]*$/d' gitlab.rb
```


An alternative for `gitlab.rb` and friends:

```bash
grep -v '^#' gitlab-rb.txt  | gsed  's/#.*$//;/^$/d'
```

## Replaces commas with a newline character

Context: I had a log file that was many MB in size but `wc` reported `0` lines. Why? How? Unknown. Using `grep` is very painful so let's fix the log. 

```shell
tr , '\n' < production.log
```

That's **OK**. 

```shell
sed 's/, "/\
/g' production.log
```


## (Silently) Automates the creation of SSH keys

⚠️ Use with extreme caution. This [suggestion](https://stackoverflow.com/questions/43235179/how-to-execute-ssh-keygen-without-prompt) **will** overwrite `~/.ssh/id_rsa` so modify to suit!


```bash
ssh-keygen -t rsa -N '' -f ~/.ssh/id_rsa <<< y
```

In order to test something once, I used this `for` loop with `seq` to automate the creation of a bunch of SSH key pairs:

```shell
for i in $(seq 1 24) ; do echo $i;  mkdir -p ohwowwhoa$i;  ssh-keygen  -t rsa -N '' -f ./ohwowwhoa$i/id_rsa$i <<< y ; done
```


### Miscellaneous Stuff

```
task export | task2dot | dot -Tsvg > test.svg
```


```
task export | task2dot | dot -Tsvg > test.svg && open test.svg
```


#### ripgrep

```
rg  -g '!{locale/*}'  'or sign in with'
```


[source](https://github.com/BurntSushi/ripgrep/issues/839#issuecomment-1006723597)

For better `df` output on macOS:

```
df -P -h
```
