# 🔗 CMOC Quick Links

## 🫶 Before

  - [ ] Check for the issue left by the [CMOC before you](https://about.gitlab.com/handbook/support/workflows/cmoc_workflows.html#end-of-shift-handover-procedure). 
	  - [ ] Mark yourself **READY TO GO!** and close the issue. 
  - [ ] Check who is the [CMOC after you](https://gitlab.pagerduty.com). 
  - [ ] Know [who the EOC](https://about.gitlab.com/handbook/engineering/infrastructure/incident-management/#who-is-the-current-eoc) is during your shift: `/chatops run oncall prod`
  - [ ] Make sure you can log in to `gitlab.com` and the like without needing to 2FA. 
	  - [ ] Make sure your 2FA device is nearby and available in case you do need to 2FA. 

## 😽 During
  - Consider using the [CMOC runbook](https://gitlab.com/gitlab-com/support/emergency-runbook/-/blob/master/.gitlab/issue_templates/Support%20-%20CMOC.md) issue template. 
### 🔖 Bookmarks

  - Support: [How to Perform CMOC Duties](https://about.gitlab.com/handbook/support/workflows/cmoc_workflows.html)
	  - [CMOC Performance Indicators](https://about.gitlab.com/handbook/support/workflows/cmoc_workflows.html#cmoc-performance-indicators)
  - Infrastructure: [Incident Management](https://about.gitlab.com/handbook/engineering/infrastructure/incident-management/)
	  - [CMOC Responsibilities](https://about.gitlab.com/handbook/engineering/infrastructure/incident-management/#communications-manager-on-call-cmoc-responsibilities) - what Infrastructure and all of GitLab expects of the CMOC
	  - [Production](https://gitlab.com/gitlab-com/gl-infra/production/-/issues) issue tracker
	  - [GitLab.com current production architecture](https://about.gitlab.com/handbook/engineering/infrastructure/production/architecture/#gitlab-com-architecture)

### 💬 Slack

  - Be in [#incident-management](https://gitlab.slack.com/archives/CB7P5CJS1). The link to the Zoom room is there. 

### 🚨 I just got paged. 

 Breathe, make sure you have water, `ack` the page and [**engage**](https://about.gitlab.com/handbook/support/workflows/cmoc_workflows.html#stage-1-engage). 

Here are the things you need to do to get from **Acknowledged** to **Resolved** on the page. 

#### 📃 Handling the Page

After you `Acknowledge` the page:

  - [ ] [Join the incident channel in Slack and the Zoom room](https://about.gitlab.com/handbook/support/workflows/cmoc_workflows.html#join-incident-channel--zoom).
  - [ ] [Create the incident](https://about.gitlab.com/handbook/support/workflows/cmoc_workflows.html#create-incident) -- as appropriate.
  - [ ] [Notify stakeholders](https://about.gitlab.com/handbook/support/workflows/cmoc_workflows.html#notify-stakeholders). 
	  - [ ] From within the [#support_gitlab-com](https://gitlab.slack.com/archives/C4XFU81LG) Slack channel, use the lightning bolt near the ➕ symbol to access the **Incident Notifier** Slack workflow. When asked for the **Slack channel**, use the Slack channel that the incident is happening in. 
  - [ ] [Label the incident issue](https://about.gitlab.com/handbook/support/workflows/cmoc_workflows.html#label-incident-issue). 
  - [ ] [Resolve the page](https://about.gitlab.com/handbook/support/workflows/cmoc_workflows.html#resolve-the-pagerduty-page). 

Nicely done! ✅ Proceed to [managing the incident](https://about.gitlab.com/handbook/support/workflows/cmoc_workflows.html#stage-2-manage). 

#### 🍝 Copy and Paste

> Use `/incident post-statuspage` on Slack to create an incident on Status.io.

Zoom: Join the Zoom room and adjust your Zoom name to **CMOC - Brie Carranza**. 

Slack: CMOC here, thread for comms 🧵.

![:thread:](https://a.slack-edge.com/production-standard-emoji-assets/14.0/apple-medium/1f9f5@2x.png)

### ✍️ Drafting a message. 

See the [what if I don't know what to say?](https://about.gitlab.com/handbook/support/workflows/cmoc_workflows.html#what-if-i-dont-know-what-to-say) section and [your notes from the training]() to guide the message. See information about [states and status](https://about.gitlab.com/handbook/engineering/infrastructure/incident-management/#states-and-statuses). 

See the table in the [Frequency of Updates](https://about.gitlab.com/handbook/support/workflows/cmoc_workflows.html#frequency-of-updates) section for more information about how frequently you should post an update based on the combination of the [state and status](https://about.gitlab.com/handbook/engineering/infrastructure/incident-management/#states-and-statuses) of the incident. 


## 🌞 After

> It's the end of your day as CMOC.

🎉 You made it! 🎉

  - [ ] Perform the [End of Shift Handover Procedure](https://about.gitlab.com/handbook/support/workflows/cmoc_workflows.html#end-of-shift-handover-procedure)  by creating a new issue in the [CMOC Handover](https://gitlab.com/gitlab-com/support/dotcom/cmoc-handover/-/issues) issue tracker.
  - [ ] Drink water and get some exercise. 