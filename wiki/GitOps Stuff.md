
  
  
  
  - [Flux](https://docs.gitlab.com/ee/user/clusters/agent/gitops/flux.html)
  - Check out the [Shall we switch to flux?](https://gitlab.com/gitlab-org/gitlab/-/issues/357947/) issue.
  - [GitOps with GitLab: What you need to know about the Flux CD integration](https://about.gitlab.com/blog/2023/02/08/why-did-we-choose-to-integrate-fluxcd-with-gitlab/)
  - Tutorial: [Set up Flux for GitOps](https://docs.gitlab.com/ee/user/clusters/agent/gitops/flux_tutorial.html)


  - Don't use a group access token!

## Using Flux

A few quick notes from May 2023.


```shell
flux logs --level=error
```


  - Use `flux uninstall` to make it go away.

### Bootstrapping

Let's look at the `flux bootstrap gitlab` output:


```
flux bootstrap gitlab \
  --owner=gitlab-gold/briecarranza/gitops \
  --repository=flux-config-officehours \
  --branch=main \
  --deploy-token-auth

► connecting to https://gitlab.com
✔ repository "https://gitlab.com/gitlab-gold/briecarranza/gitops/flux-config-officehours" created
```

The `flux-config-officehours` repository we requested is created.


```
► cloning branch "main" from Git repository "https://gitlab.com/gitlab-gold/briecarranza/gitops/flux-config-officehours.git"
✔ cloned repository
```

...and cloned via HTTPS.


```
► generating component manifests
✔ generated component manifests
✔ committed sync manifests to "main" ("6bb5acfecb1ce51d81b1ca2e6cc97819271b155d")
► pushing component manifests to "https://gitlab.com/gitlab-gold/briecarranza/gitops/flux-config-officehours.git"
► installing components in "flux-system" namespace
✔ installed components
✔ reconciled components
► checking to reconcile deploy token for source secret
✔ configured deploy token "flux-system-main-flux-system" for "https://gitlab.com/gitlab-gold/briecarranza/gitops/flux-config-officehours"
► determining if source secret "flux-system/flux-system" exists
► generating source secret
► applying source secret "flux-system/flux-system"
✔ reconciled source secret
► generating sync manifests
✔ generated sync manifests
✔ committed sync manifests to "main" ("db9477be9903116bcc464f59943f8633c4380752")
► pushing sync manifests to "https://gitlab.com/gitlab-gold/briecarranza/gitops/flux-config-officehours.git"
```

Eventually, Flux switches from using `HTTPS` to using `SSH` for some `git` operations.


```
► applying sync manifests
✔ reconciled sync configuration
◎ waiting for Kustomization "flux-system/flux-system" to be reconciled
✔ Kustomization reconciled successfully
► confirming components are healthy
✔ helm-controller: deployment ready
✔ kustomize-controller: deployment ready
✔ notification-controller: deployment ready
✔ source-controller: deployment ready
✔ all components are healthy
```


The 🔐 private [gitlab-gold/briecarranza/gitops/flux-config-officehours](https://gitlab.com/gitlab-gold/briecarranza/gitops/flux-config-officehours) project was created.


See the [Bootstrap with http instead of ssh](https://github.com/fluxcd/flux2/issues/2201) issue, which notes:

> PS. using `bootstrap github --token-auth` does what you want. You need to delete the `flux-system` secret before rerunning bootstrap and flux will generate the secret with https credentials.



#### HTTPS vs SSH


 See the [Add --deploy-token option to Flux bootstrap](https://gitlab.com/gitlab-org/gitlab/-/issues/392605) issue.

That issue give us this quote from [an upstream issue](https://github.com/fluxcd/flux2/discussions/3595):

> > add a `--deploy-token` optional arg to flux bootstrap gitlab, when this is set, then bootstrap switches to HTTP/S and sets the token in the secret, in the same manner it does today for --token-auth

The answer might be:

- Start over
- Use the `--deploy-token-auth` flag from the docs
- Check the contents of the `flux-system` secret


#### Checking the `flux-system` secret

```shell
kubectl get secrets --namespace=flux-system
```


```shell
kubectl --namespace=flux-system describe secret flux-system
```

```shell
kubectl --namespace=flux-system get secret flux-system -o jsonpath='{.data}'
```


## History

  - certificate-based integration
	  - deprecated with GitLab 14.5 | [blog post](https://about.gitlab.com/blog/2021/11/15/deprecating-the-cert-based-kubernetes-integration/)
	  - Epic: [Deprecation of the certificate-based integration with Kubernetes](https://gitlab.com/groups/gitlab-org/configure/-/epics/8)
  - GitLab agent for Kubernetes
  - Flux