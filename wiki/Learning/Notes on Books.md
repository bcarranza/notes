
## The Staff Engineer's Path



I started reading [The Staff Engineer's Path](). It's awesome. 

### Introduction

A few things that _really_ called to me:


> Engineers considering this path may have never worked with a staff engineer before, or might have seen such a narrow set of personalities in the role that it seems like unattainable wizardry.

Basically, the lesson is:

  - **Embrace** uncertainty. 

Three pillars of staff engineering are offered:

 - Big-picture thinking
	 - think beyond the current time
	 - see the forest but sometimes....you'll need to closely inpsect a tree
 - Execution
	 - projects get messier and more ambiguous, they'll involve more people and be _harder_
 - Leveling up (the folks you work with)
	 - intentional influence: teaching, mentoring
	 - accidental influence: being a role model/example


The rest of the book is broken up into parts based on those three pillars. 



Based on the links:

  - https://staffeng.com/
  - [An incomplete list of skills senior engineers need, beyond coding](https://skamille.medium.com/an-incomplete-list-of-skills-senior-engineers-need-beyond-coding-8ed4a521b29f)