---
module-name: "GraphQL"
area: "Product Knowledge"
gitlab-group: ""
maintainer:
  - Brie Carranza - @bcarranza
---

---
# GraphQL

> A while back, I did a self-led "30 days of GraphQL" and it turned out to be _super_ useful. I find myself chatting with folks about GraphQL in pairing sessions. It's useful for troubleshooting and for better understanding how GitLab works. In this file, I am drafting a GraphQL training module aimed at GitLab Support Engineers with little to no experience with GraphQL. 


---

## Stage 0: Create and commit to the module

1. [ ] Create an issue using this template by making the Issue Title: GraphQL - "your name"
1. [ ] Add yourself and your trainer (if you have one) as the assignees. Feel free to ping the maintainer(s) of this training module to see if they can help you with this module.
	1. Consider setting up a pairing session to spend some time using GraphQL together.
2. [ ] Notify your manager to let them know you've started.
3. [ ] Consider setting up a self-managed GitLab instance in [GitLab Sandbox Cloud](https://gitlabsandbox.cloud) with an **Ultimate** license in order to follow along with all of the demos. (It's not necessary: if you see the 🖥️  emoji, that step requires a test instance.)
4. Optional: Set a milestone, if applicable, and a due date to help motivate yourself!

## Stage 1: What _is_ GraphQL?

> In this stage, we get a high-level understanding of what GraphQL is, why anyone would use it and some of the basic terms to be familiar with. 

  - What even _is_ GraphQL?
	  - It's an alternative to REST.
	  - With GraphQL queries, you describe the data that you want and the response returns only the data you need. In REST, you have to parse through the entire response on the client side.
	  - You can read and change data via GraphQL.
	  - [ELI5: graphql](https://eli5.gg/graphql)
	  - [ ] Read [About GraphQL](https://graphql.braintreepayments.com/guides/#about-graphql)
	  - [ ] Read [GraphQL: A data query language](https://engineering.fb.com/2015/09/14/core-data/graphql-a-data-query-language/)
		  -  This 2015 post from Meta (then Facebook) is super short and describes some of the challenges that they solved with GraphQL. Understanding why Meta built GraphQL provides helpful context on why it's so broadly adopted today, more than ten years after it was first built.
		  - GraphQL was made public in 2015. 
		  - [ ] _Optional_ Read [What is GraphQL? Part 1: The Facebook Years](https://blog.postman.com/what-is-graphql-part-one-the-facebook-years/)
  - Why would anyone use it?
	  - Read [Introducing the Salesforce GraphQL API](https://developer.salesforce.com/blogs/2022/03/introducing-the-salesforce-graphql-api) especially the **Developers lack control with traditional REST APIs** section.

### The Basics of GraphQL

These are some basic GraphQL terms to be familiar with:

  - queries
	  - This is how you retrieve data. Think of a `GET` request when using a REST API.
  - mutations
	  - This is how you can change things. Think of a `POST` request when using a REST API.
  - resolvers
	  - This is how the data for your queries is retrieved. In GitLab, these will be functions in the GitLab source code. 
  - authentication
	  - typically very common to authentication with REST APIs

### REST vs GraphQL in GitLab
  - [ ] Take a look at these examples of GraphQL usage in the GitLab source code
  - [ ] Run through the demo below to see the difference between REST and GraphQL APIs.

Here's a quick demo to illustrate how GraphQL can be so helpful: let's say we want to get a user's **name** and nothing else!

  1. When you are already logged in to `gitlab.com`, go to `https://gitlab.com/api/v4/user`.
  2. Observe that the `name` is there but there is a lot of extra information that we don't really want
  3. Head over to [the GraphQL explorer](https://gitlab.com/-/graphql-explorer)
  4. Issue this query: `{currentUser {name}}`
  5. Observe that the `name` is returned with none of the extra information we saw before

## Stage 2: Hands on with GraphQL and GitLab

  - [ ] Read about [Global IDs](https://docs.gitlab.com/ee/development/api_graphql_styleguide.html#global-ids) to understand how to reference things like users (`gid://gitlab/User/0123456789`), projects(`gid://gitlab/Project/000000`), snippets () and more! 
	  - You might retrieve the project ID via logs, the Web interface or a screenshot. 

We'll cover different ways to issue GraphQL queries and mutations:

  - "API clients" like `cURL`, Insomnia, Postman
  - [GraphiQL](https://docs.gitlab.com/ee/api/graphql/getting_started.html#graphiql) -- recommended way to get started
	  - Observe the auto-complete! You don't need to read through the entire reference.
  - In Rails Console!:  https://docs.gitlab.com/ee/api/graphql/getting_started.html#rails-console
  - GraphQL is baked into GitLab in interesting places:
	  - link to examples with FE queries
	  - When we're iterating, an early feature may only be available via the GraphQL API to start, as with [audit event streaming](https://docs.gitlab.com/ee/administration/audit_event_streaming.html).
- [ ] Consider joining the `#graphql` and `#f_graphql` Slack channels. 
 

### Recommended Reading

  - micro course on [GitLab's GraphQL API](https://levelup.gitlab.com/learn/course/gitlabs-graphql-api/main/gitlabs-graphql-api?client=internal-team-members) 
  - Reference: https://docs.gitlab.com/ee/api/graphql/reference/

### Examples (with emoji!)

  - https://docs.gitlab.com/ee/api/graphql/custom_emoji.html
  - https://docs.gitlab.com/ee/administration/audit_event_streaming.html
	  - **Ultimate**


## Stage 3: GraphQL for Troubleshooting

GraphQL can be _super_ helpful for troubleshooting all kinds of things in GitLab!

  - Maybe you want to write snippets for the GitLab Rails Console **but** writing Ruby is not your strength: you can send GraphQL queries and mutations via the GitLab Rails Console.
  - Maybe the REST API is not working as expected and you'd like to see if the corresponding query or mutation is working via the GraphQL API:
	  - If **yes**: you have a workaround! 
	  - If **no**: you have helped to clarify the problem statement and to start to isolate the cause of the problem

  - [ ] Getting errors
  - [ ] Understanding how GraphQL responds
	  - [ ] You might see `null`. The problem could be the token. GraphQL tends not to be super verbose about this.
- [ ] Using GraphQL mutations to change things in GitLab
	- [ ] Assign a policy project to enable [scan result policies](https://docs.gitlab.com/ee/user/application_security/policies/scan-result-policies.html)
		- [ ] ℹ [GitLab] Assign a security policy project via GraphQL. Use the [securityPolicyProjectAssign](https://docs.gitlab.com/ee/api/graphql/reference/#mutationsecuritypolicyprojectassign) mutation.
	- [ ] 🖥️  Create an [audit event streaming destination](https://docs.gitlab.com/ee/administration/audit_event_streaming.html#use-the-api)
	- [ ] [Delete uploaded files](https://docs.gitlab.com/ee/security/user_file_uploads.html#delete-uploaded-files)

Here's an example mutation showing how to assign a security policy project:

```

mutation {
  securityPolicyProjectAssign(
    input: {fullPath: "bcarranza/detecting-secrets", securityPolicyProjectId: "gid://gitlab/Project/000000"}
  ) {
    clientMutationId
    errors
  }
}
```


### GraphQL Tools

  - [ ] Consider installing [Insomnia](https://insomnia.rest) or [Postman](https://www.postman.com/) for use as a GraphQL client
	  -  Issue a query to the GitLab GraphQL API using the tool you selected
		  - [ ] Record the query and results in a comment on this issue
	  - [ ] Issue a GraphQL mutation to the GitLab GraphQL API using the tool you selected
	  - [ ] Issue a similar mutation via the GitLab Rails console
- [ ] You can also use `curl`

  - https://chrome.google.com/webstore/detail/graphql-network-inspector/ndlbedplllcgconngcnfmkadhokfaaln?hl=en-GB

## Stage 4: Building Better Queries

There are a lot of really cool things you can do with GraphQL. So far, we have just built some pretty simple queries. Let's do a bit more! 

  - [ ] Use the GraphQL reference and the auto-complete support in [GraphQL Explorer](https://gitlab.com/-/graphql-explorer) to build a query for some data that interests you. 
	  - [ ] Watch [this recording]() for an example of how to build a GraphQL query in this way.


## Stage 5: Beyond!

> This is _optional_ but if you're finding this all super fun and interesting, here's more! Try one, try a few or do them all!

- [ ] Do something that will trigger a GraphQL query and use the information in [Monitoring GraphQL](https://docs.gitlab.com/ee/development/graphql_guide/monitoring.html) to find the logs in Kibana or in your self-managed instance. Record the results in a comment below.
- [ ] Read about [query multiplexing](https://graphql-ruby.org/queries/multiplex.html)! 
- [ ] Look through the [GraphQL API style guide](https://docs.gitlab.com/ee/development/api_graphql_styleguide.html).
- [ ] Fork the [GraphQL DAST example project](https://gitlab.com/gitlab-org/security-products/demos/api-dast/graphql-example)
	- [ ] Browse through the source code, especially `schema.py`
	- [ ] Run a pipeline and take a look at the results
- [ ] Experiment with issuing queries to [GraphQLZero](https://graphqlzero.almansi.me/) a fake online GraphQL API for testing 
- [ ] Read [An Introduction to GraphQL and Rubrik](https://www.rubrik.com/content/dam/rubrik/en/resources/white-paper/an-introduction-to-graphql-and-rubrik.pdf)
- [ ] Check out the [GraphQL Foundation](https://www.youtube.com/@GraphQLFoundation) on YouTube. They meet regularly and make [their notes available](https://github.com/graphql/graphql-wg).


## Penultimate stage: Review

Any updates or improvements needed? If there are any dead links, out of date or inaccurate content, missing content whether in this module or in other documentation, list it below as tasks for yourself! Once ready, have a [maintainer or manager review](https://gitlab.com/gitlab-com/support/support-training#guideline-to-update-support-training-module).

1. [ ] Update ...

## Final stage: Completion

1. [ ] Have your trainer review your tickets and assessment. If you do not have a trainer, ask an expert to review.
1. [ ] Manager: schedule a call (or integrate into 1:1) to review how the module went.
1. [ ] Submit a MR to update `modules` and `knowledge_areas` in the [Support Team yaml file](https://gitlab.com/gitlab-com/support/team/-/blob/master/data/support-team.yaml) with this training module's topic. You will now be listed as an expert in **GraphQL**  on the GitLab Support Team [Skills by Person page](https://gitlab-com.gitlab.io/support/team/skills-by-person.html). 