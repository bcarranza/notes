
In the `server` section, you must specify `ssl` at the end of the `listen` directive:

[Syntax:	listen](http://nginx.org/en/docs/http/ngx_http_core_module.html#listen)

