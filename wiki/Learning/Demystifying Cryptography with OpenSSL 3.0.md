
- [ ] 1
- [ ] 2
- [ ] skim 3
- [ ] skim 4
- [ ] 7
- [ ] 8
- [ ] 9

https://github.com/PacktPublishing/Demystifying-Cryptography-with-OpenSSL-3/tree/main?tab=readme-ov-file

# OpenSSL and Friends


Compare OpenSSL with GnuTLS, NSS (formerly Hard Core Library -- HCL, Botan, LibreSSL (fork from the Heartbleed era), BoringSSL, the lightweights (wolfSSL, MatrixSSL, etc).

😅 Yes, it's _called_ OpenSSL but we're probably talking about `TLS` these days.

### OpenSSL 3.0

> The OpenSSL project used its BSD-style open source license until version 3.0. Since version 3.0, it uses Apache License 2.0.

Also:

* We get OpenSSL operation implementation providers and Kernel TLS (KTLS) to support offloading.
* Support for CMP.
* An HTTP/HTTPS client. 
* Interactive mode for the `openssl` command line tool goes away
	* Do we tell folks to use that anywhere in the docs?

See [Other major new features](https://docs.openssl.org/3.0/man7/migration_guide/#other-major-new-features).

- [Guide to Testing an SSL Connection Using OpenSSL](https://www.liquidweb.com/blog/how-to-test-ssl-connection-using-openssl/)

> ##### Interactive mode from the **openssl** program has been removed[¶](https://docs.openssl.org/3.0/man7/migration_guide/#interactive-mode-from-the-openssl-program-has-been-removed "Permanent link")
> 
> From now on, running it without arguments is equivalent to **openssl help**.


```shell
# openssl --version                                              
OpenSSL 3.4.0 22 Oct 2024 (Library: OpenSSL 3.4.0 22 Oct 2024)
```

```shell
echo Q | timeout 1 openssl s_client -connect gtlb.catsilove.com:443 -tls1_3 -verify_hostname gtlb.catsilove.com -x509_strict
```

😅 [cert2json](https://github.com/philwantsfish/cert2json)
- [Parsing X.509 Certificates with OpenSSL and C](https://zakird.com/2013/10/13/certificate-parsing-with-openssl)
- [jc.parsers.x509_cert](https://kellyjonbrazil.github.io/jc/docs/parsers/x509_cert.html)
- 

🤭 Skipping the section on **Symmetric Cryptography**. 


### Asymmetric Cryptography and Certificates

#### Digital Signatures and Their Verification


