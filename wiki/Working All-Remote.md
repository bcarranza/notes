Here are a few tools and techniques that have been super helpful for me as I have embraced the all-remote life. 

## Sounds

> Having the right sound is everything! Here are the things I use the most. 

  - [Rainy Mood](https://rainymood.com/)
	  - I use the [Rainy Mood app](https://play.google.com/store/apps/details?id=com.TailoredMusic.RainyMood) underneath podcasts, audiobooks and playlists for a nice calming vibe. 
  - [brain.fm](https://brain.fm)
  - It's been a while but [musicforprogramming.net](https://musicforprogramming.net/about) is pretty cool. See [DATASETTE](http://datassette.net/).
  - LOFI
	  - Find [Lofi Girl](https://lofigirl.com/). 
	  - Go to **Spotify**, search for `lofi` plus anything, listen, enjoy. 
		  - `lofi beach`
		  - `lofi cafe`
		  - `lofi edm`
		  - `lofi classical`

### To Test


## Body Doubling

Whatever you call it, it's helpful (for me)!

  - [Study with BTS](https://www.youtube.com/watch?v=TBJVvyMBDdk) | Free, 20 minute video 💜💜💜💜💜💜💜
  - [Focusmate](https://focusmate.com) | Free: up to X sessions, $__ after that
  - [Flow Club](https://flow.club) | Monthly :$40


