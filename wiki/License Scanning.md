
The [License list](https://docs.gitlab.com/ee/user/compliance/license_list.html) is populated when GitLab parses an SBOM generated for one of the supported languages. 

> License Scanning should use the `licenses` field of the CycloneDX JSON SBOM when available, and fall back to using license information imported from the external License DB.

source: [Use licenses of CycloneDX SBOMs in license scanner](https://gitlab.com/gitlab-org/gitlab/-/issues/415935)

The [Ingest SBOM reports](https://gitlab.com/groups/gitlab-org/-/epics/8024) epic has some useful info about how everything is architected.


## 🌐 External Resources

- CycloneDX [Tool Center](https://cyclonedx.org/tool-center/#tool-center)
- 
