
**GitLab Dedicated** is:

> [Deployed on AWS in a cloud region of your choice.](https://docs.gitlab.com/ee/subscriptions/gitlab_dedicated/)


There are some specific AWS cloud concepts that are especially helpful to know about when working with GitLab Dedicated. 

- [Amazon Resource Names (ARNs)](https://docs.aws.amazon.com/IAM/latest/UserGuide/reference-arns.html)
	- Amazon Resource Names (ARNs) uniquely identify AWS resources. We require an ARN when you need to specify a resource unambiguously across all of AWS, such as in IAM policies, Amazon Relational Database Service (Amazon RDS) tags, and API calls.
- [Identity and Access Management (IAM)](https://docs.aws.amazon.com/IAM/latest/UserGuide/introduction.html)
	- AWS Identity and Access Management (IAM) is a web service that helps you securely control access to AWS resources. With IAM, you can centrally manage permissions that control which AWS resources users can access. You use IAM to control who is authenticated (signed in) and authorized (has permissions) to use resources.

- PHZ - private hosted zone
	- [A private hosted zone is a container that holds information about how you want Amazon Route 53 to respond to DNS queries for a domain and its subdomains within one or more VPCs that you create with the Amazon VPC service.](https://docs.aws.amazon.com/Route53/latest/DeveloperGuide/hosted-zones-private.html)
- [AWS PrivateLink](https://aws.amazon.com/privatelink/) | Establish connectivity between VPCs and AWS services without exposing data to the internet
	- forward
	- reverse