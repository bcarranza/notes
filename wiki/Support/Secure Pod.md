- [[Parsing Secret Detection JSON]]


## Sample Projects

> Interesting sample projects related to Secure Pod topics. 

  - https://gitlab.com/gitlab-org/security-products/demos/api-dast/openapi-example



You might also want to read about troubleshooting [[DAST]].