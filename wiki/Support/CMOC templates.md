# CMOC Templates

Used to document frequent messages posted on status page.

## Investigating

We are currently investigating issues with `<COMPONENT>`. More details about this incident can be found in `gitlab.com/gitlab-com/gl-infra/production/-/issues/<NUMBER>.`

### No Updates Message

No material updates at this time - our investigation is ongoing. `<workaround-or-somewhat-important-message-here>.` More details can be found in `gitlab.com/gitlab-com/gl-infra/production/-/issues/<NUMBER>.`

## Identified

We've identified cause of the issue and are working on resolving it. `<workaround-if-possible-here>.` More details can be found in `gitlab.com/gitlab-com/gl-infra/production/-/issues/<NUMBER>.`

### no update

No current updates at this time. `<workaround-or-somewhat-important-message-here>.` More details can be found in `gitlab.com/gitlab-com/gl-infra/production/-/issues/<NUMBER>`.

## Monitoring

We have applied a fix and are now monitoring `<component>` for 1 hour before marking as resolved. More details can be found in `gitlab.com/gitlab-com/gl-infra/production/-/issues/<NUMBER>`.

## Resolved

This incident has been resolved and `<component>` is now operational. More information can be found in `gitlab.com/gitlab-com/gl-infra/production/-/issues/<NUMBER>`.





See [[CMOC]] for info on when to use these. 