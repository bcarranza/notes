# CMOC 

## Reminders

  - Breathe. 
  - You are not alone: everyone is working to fix the problem (well). 
  - Ensure you have access to the things in [[CMOC Quick Links]]. 


### Training

  - [CMOC Training](https://gitlab.com/gitlab-com/support/support-training/-/blob/master/.gitlab/issue_templates/GitLab-com%20CMOC.md) template
  - [Emergency Booster](https://gitlab.com/gitlab-com/support/support-training/-/blob/master/.gitlab/issue_templates/GitLab-com%20Emergency%20Booster.md)




## CMOC Training Session

All the links you'll need: [setting up for success](https://gitlab.com/gitlab-com/support/support-training/-/blob/master/.gitlab/issue_templates/GitLab-com%20CMOC.md#stage-6-setting-up-for-success)

[CMOC Workflows](https://about.gitlab.com/handbook/support/workflows/cmoc_workflows.html)



### August 26, 2022

Imaginary Scenario: The Runners are down. 

  1. Read the workflow before doing anything. 
  2. The incident is **Resolved** when the status page has been _created_. 


The **Incident Manager** (IM) [decides](https://about.gitlab.com/handbook/support/workflows/cmoc_workflows.html#deciding-whether-to-initiate-status-page-communications-eoc-vs-im) whether this is communicated via a status page. Always follow the workflow to the lettter. 


We update periodically to say "We are continuing to investigate." so people know we are.....continuing to investigate. 

The severity can be increased or decreased as we learn more. 


See [[CMOC templates]] for example wording.

There won't always be an incident. 



## Glossary




