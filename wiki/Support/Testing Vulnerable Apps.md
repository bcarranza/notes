

## Some Cool Stuff

- https://gitlab.cylab.be/-/snippets/2
- https://cylab.be/blog/181/secure-your-project-with-the-gitlab-sast-analyzers
- https://gitlab.cylab.be/cylab/simple-traffic-generator
- https://gitlab.cylab.be/cylab/play/vulnerable-mail-srv
- https://gitlab.cylab.be/explore/projects?non_archived=true&page=4&sort=latest_activity_desc


## Intentionally Vulnerable Code
If you're looking for an intentionally vulnerable code and Web apps, consider:

  - [OWASP Juice Shop](https://owasp.org/www-project-juice-shop/)
  - [awesome-vulnerable](https://github.com/kaiiyer/awesome-vulnerable)
  - [Awesome Vulnerable Apps](https://github.com/vavkamil/awesome-vulnerable-apps)
  - [Insecure Code Examples](https://github.com/bhrott/insecure-code-samples)
	  - July 2023: I like using these for testing **SAST**.

Check out these `.toml` files to see a list of apps for testing DAST:

  - https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/tree/main/configs

## DAST Example Projects

Check out these projects for live examples of intentionally vulnerable apps being tested by GitLab's DAST offering in particular:

  - [DAST Demos](https://gitlab.com/gitlab-org/security-products/demos/dast)


