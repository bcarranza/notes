When troubleshooting problems with [[Advanced Search]], you may get to a point where you have to recreate the index. There are two ways to recreate the index used by Elasticsearch (or Opensearch). The options are:

  - zero downtime reindexing AKA cluster reindexing
	  - Docs: [Zero downtime reindexing](https://docs.gitlab.com/ee/integration/advanced_search/elasticsearch.html#zero-downtime-reindexing) for Elasticsearch. This is completely different from the zero downtime upgrades associated with [[Geo]].
	  - See the [cluster reindexing service](https://gitlab.com/gitlab-org/gitlab/-/blob/master/ee/app/services/elastic/cluster_reindexing_service.rb). 
  - reindexing
	  - Run `gitlab-rake gitlab:elastic:index`
	  - Deletes the old index first. As a result, should be used as a last resort. There is no index to fall back on while the operation runs. 

That supports why we say recreating the index is a last resort: there's nothing to fall back on when you're doing that (the `gitlab-rake gitlab:elastic:index` method).