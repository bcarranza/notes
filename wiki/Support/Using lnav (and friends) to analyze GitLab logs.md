# lnav

- [The Logfile Navigator](https://lnav.org/)

## Sidekiq

> Logs from Sidekiq pods. 

It's not smart enough to detect the `INFO` and `WARN`. I like this for logs from the Sidekiq pod instead to get the logs:

```
# grep '^{' sidekiq.log > sidekiq.json
# jless sidekiq.json
```

See [[Sidekiq]] for notes on analyzing those logs.

## Friends

- [[gron]]
