> November 2022

SAML: synchronization happens at login

  - So `extern_uid` and `NameID` are the same thing. 

The sync with the IdP happens at login. 

GitLab requests and receives a SAML response. 

The account only gets created once the user logs in. (SCIM works differently.)

Don't use `Transient`.

You only **need** Email but the names can be used to auto populate the names.

We want the SHA1; if you give the SHA-256 it will complain you have the incorrect fingerprint (`fingerprintmismatch`).

