# GitLab: Offer TLS 1.3 Only

## Testing TLS

- `openssl`
- [testssl.sh](https://testssl.sh/)

[Luckily for us, the TLSv1.3 RFC supports only 5 cipher suites](https://isc.sans.edu/diary/Testing+TLSv13+and+supported+ciphers/25442)

```
+------------------------------+-------------+
| Description                  | Value       |
+------------------------------+-------------+
| TLS_AES_128_GCM_SHA256       | {0x13,0x01} |
| TLS_AES_256_GCM_SHA384       | {0x13,0x02} |
| TLS_CHACHA20_POLY1305_SHA256 | {0x13,0x03} |
| TLS_AES_128_CCM_SHA256       | {0x13,0x04} |
| TLS_AES_128_CCM_8_SHA256     | {0x13,0x05} |
+------------------------------+-------------+    
```

```
openssl s_client -tls1_3 -connect www.cloudflare.com:443
```

```
openssl s_client -tls1_3 -connect dev.ops.brie.lol:443
```

```
for cipher in TLS_AES_128_GCM_SHA256 TLS_AES_256_GCM_SHA384 TLS_CHACHA20_POLY1305_SHA256 TLS_AES_128_CCM_SHA256 TLS_AES_128_CCM_8_SHA256 ; do openssl s_client -tls1_3 -ciphersuites $cipher -connect dev.ops.brie.lol:443 < /dev/null > /dev/null 2>&1 && echo "$cipher" ; done
```

## Deploy TLS 1.3 _only_

GitLab 16.8

```
##! **Recommended by: https://raymii.org/s/tutorials/Strong_SSL_Security_On_nginx.html
##!                   https://cipherli.st/**
nginx['ssl_protocols'] = "TLSv1.3"
```

### Sources

[this comment](https://gitlab.com/gitlab-org/omnibus-gitlab/-/issues/3262#note_62450411)
