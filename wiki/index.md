# Home

:wave: Hello! These are my notes. The source for this site is available [in the `bcarranza/notes` project](https://gitlab.com/bcarranza/notes) on `gitlab.com`. This is (the latest iteration of) my digital garden at work. These are **evergreen** notes. Like [everything](https://about.gitlab.com/handbook/values/#everything-is-in-draft), these notes and this site are in draft form. This enables me to get started with scribbling things down and worrying about the rest later. These notes are available at [notes.brie.dev](https://notes.brie.dev). 

## `whoami`

Oh, hi! My name is **Brie** Carranza. I am a **Staff Support Engineer** at GitLab. I joined GitLab in May 2020, just after [GitLab 13.0](https://about.gitlab.com/releases/2020/05/22/gitlab-13-0-released/) was released. 🦊 💜 

## :sparkles: 

> These are some of my best notes...

- [PostgreSQL Training: Part 1 // 2021](https://hackmd.io/2bHfFb8dScWioiNZ8L3wIw#Day-3-Memory-and-cache-management--Backup-and-restore)

## :key: Topics

  - [[graphql|GraphQL]]
	  - [[GraphQL Queries for Troubleshooting]]
  - [[Pipeline Insights]]
  - [[lfs]]
  - [[Advice]]

## :bookmark: Bookmarks
  - [[One Liners]]

### 🌎 External

- [Upcoming releases](https://about.gitlab.com/releases/#upcoming-releases)
- [Product sections, stages, groups, and categories](https://handbook.gitlab.com/handbook/product/categories/)
- [Feedback template](https://handbook.gitlab.com/handbook/product/product-management/#feedback-template)

### Useful Tools

  - [One-line HTTP servers](https://www.devdungeon.com/content/one-line-http-servers)
  - [That Command](https://thatcommand.com/)
  - [[Toolbox]]
  - [[Working All-Remote]]

## Links

Every now and then, something rings a bell. Sometimes it's relevant, sometimes it's not. To speed the time to find the ringing bell, see [[Rang a Bell]].

  - [[Bug Reports]]

### Python Web Server w/ HTTP Basic Authentication

I'm looking at these options:

  - https://yaler.net/simple-python-web-server
  - https://gist.github.com/dragermrb/108158f5a284b5fba806
  - https://gist.github.com/fxsjy/5465353


---

I mark Geo-related issues of interest with the `:globe_with_meridians: ` emoji. I then use [this link](https://gitlab.com/gitlab-org/gitlab/-/issues/?sort=created_date&state=opened&my_reaction_emoji=globe_with_meridians&first_page_size=20) to filter for issues where `My-Reaction` is that emoji (🌐)). 

Here's a list of the emoji I use in issues:

  - 💡 - Pipeline Insights
  - 🉐 - GraphQL
	  - This is the _Japanese “Bargain” Button_ emoji which is referred to as `:ideograph_advantage:`.


### Projects

> Projects I use often...
> ...also projects I don't use often but want to be able to find quickly when I need them. 

  - [bcarranza/detecting-secrets](https://gitlab.com/bcarranza/detecting-secrets) - my test project for **Secret Detection**
  - [bcarranza/configs](https://gitlab.com/bcarranza/configs) - example configs
	  - mostly for GitLab test environments that are not in GET or `gitlabsandbox.cloud`
	  - [[Configs for CLI Tools]]
  - [gitlab-com/support/toolbox/gitlab-smoke-tests](https://gitlab.com/gitlab-com/support/toolbox/gitlab-smoke-tests)
  - [bcarranza/openapi](https://gitlab.com/bcarranza/openapi)

There are more example projects on the [[Secure Pod]] page. 

#### Go-To Test Projects

These are the test projects I go to very often. 

  - [bcarranza/detecting-secrets](https://gitlab.com/bcarranza/detecting-secrets) - my test project for **Secret Detection**
  - [bcarranza/such-quality](https://gitlab.com/gitlab-gold/briecarranza/such-quality)
	  - [example MR](https://gitlab.com/gitlab-gold/briecarranza/such-quality/-/merge_requests/2)
  - [Container Scanning Test](https://gitlab.com/gitlab-org/govern/demos/container-scanning-test)

## Miscellaneous

- [[Interesting Stuff]]

https://circleci.com/blog/smoke-tests-in-cicd-pipelines/

---


# Behind the Scenes

I write the Markdown for this site (mostly) in [Obsidian](https://obsidian.md/).

> Obsidian is a powerful **knowledge base** on top of  
a **local folder** of plain text Markdown files.


That Markdown is converted to HTML and deployed to GitLab Pages via GitLab CI. The job that does the ✨ magic ✨  uses [MkDocs](https://www.mkdocs.org/). While this set up works quite well for me, there are a few downsides. 

  - Obsidian is [not currently open source](https://forum.obsidian.md/t/open-sourcing-of-obsidian/1515). 
  - I permit "mega commits". I use bad commit messages and commit a bunch of files all at once. 
	  - I do that here in the spirit of getting things out of my head and in my notes site where they can be useful to others as quickly as possible. 
  - Some page names have spaces in them. 
	  - 👻😨😱😯😮😧
	  - I don't like this but...here we are. 
	  - Maybe I'll move to something _like_ [StandardNotes](https://github.com/standardnotes/app) one day but I like pretty things, enough to accept URLs with `%20`.  
