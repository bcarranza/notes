# API Clients

In 2024, I finally moved from Insomnia to Bruno. 

**READ** [the saas dilemma](https://www.usebruno.com/blog/the-saas-dilemma) to learn why. 

I don't use it currently but [Restfox](https://github.com/flawiddsouza/Restfox) purprots to solve the same problem. 

StepCI looks cool especially since it supports [integrating with GitLab CI/CD](https://docs.stepci.com/integration/gitlab.html). That's a slightly different use case but still interesting. 

## Interesting Calls

Here are a cool API calls that I wanted to save when migrating away from Insomnia:

```
curl --request POST \
  --url 'https://gitlab.com/api/v4/projects/48039008/protected_branches?name=cutestcatWTH&push_access_level=30&unprotect_access_level=40' \
```

```
curl --request POST \
  --url https://gitlab.brie.run/api/v4/projects \
  --header 'Content-Type: application/json' \
  --data '{
        "name": "new_project", "description": "New Project", "path": "new_project",
        "namespace_id": "19", "initialize_with_readme": "true"}'
```

```
curl --request POST \
  --url https://gitlab.brie.run/api/v4/projects \
  --header 'Content-Type: application/json' \
  --data '{
        "name": "new_project", "description": "New Project", "path": "new_project",
        "namespace_id": "19", "initialize_with_readme": "true"}'
```

Turn on Maintenance Mode

```
curl --request PUT \
  --url 'https://gitlab.brie.dev/api/v4/application/settings?maintenance_mode=true&maintenance_mode_message=We%20are%20upgrading%20GitLab%20right%20now.' \
```

Schedule storage moves:

```
curl --request POST \
  --url https://omg.brie.dev/api/v4/groups/4/repository_storage_moves \
  --header 'Content-Type: application/json' \
  --data '{"destination_storage_name":"storage01"}'
```


