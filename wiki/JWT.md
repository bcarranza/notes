# JSON Web Tokens

> The suggested pronunciation of JWT is the same as the English word "jot".

src: [RFC 7519](https://datatracker.ietf.org/doc/html/rfc7519#section-1)

## 🔖 Bookmarkable Sites

- [jwt.io](https://jwt.io/)
- [mkjwk | simple JSON Web Key generator](https://mkjwk.org/)
