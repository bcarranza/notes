# Parsing SBOMs

> This note includes information about parsing SBOMs in the [GitLab Rails Console](https://docs.gitlab.com/ee/administration/operations/rails_console.html). 

## HOWTO 

The docs describe that **Dependency Scanning** supports [select languages and package managers](https://docs.gitlab.com/ee/user/application_security/dependency_scanning/#supported-languages-and-package-managers). The `gemnasium` project has notes on adding a new [dependency file parser](https://gitlab.com/gitlab-org/security-products/analyzers/gemnasium/-/tree/master?ref_type=heads#dependency-file-parsers). Check out the [existing parsers](https://gitlab.com/gitlab-org/security-products/analyzers/gemnasium/-/tree/master/scanner/parser).  

On [line 66](https://gitlab.com/gitlab-org/security-products/analyzers/gemnasium/-/blob/master/cyclonedx/convert.go?ref_type=heads#L66) of `convert.go`, we see that `ToSBOMs`:

> ```
> // ToSBOMs converts dependency files to CycloneDX SBOMs, with components in sorted order
> ```

A quick start to parsing a CycloneDX SBOM `.json` file as GitLab would:

```ruby
file = File.read 'gl-sbom-report.cdx.json'
data_hash = JSON.parse(file)
raw_report_data = data_hash.to_json
report = Gitlab::Ci::Reports::Sbom::Report.new
raw_report_data
r = Gitlab::Ci::Parsers::Sbom::Cyclonedx.new.parse!(raw_report_data, report)
report
report.components
```

[src](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/140042#how-to-set-up-and-validate-locally)

## Resources

### Issues and MRs

- **Merged** [Add props field to SBOM component parser](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/140042)

### External

- [OWASP CycloneDX is a full-stack Bill of Materials (BOM) standard](https://cyclonedx.org/)

### GitLab Docs
- [License list](https://docs.gitlab.com/ee/user/compliance/license_list.html)
- [Supported languages and package managers](https://docs.gitlab.com/ee/user/application_security/dependency_scanning/#supported-languages-and-package-managers)
