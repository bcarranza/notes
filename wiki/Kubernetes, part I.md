I started this in mid-January 2022. I already have a handle on some [[Kubernetes]] basics. See my notes on [[Runner on Kubernetes]] for a bit more. 

## Stage 1

```
helm install bitnami/mysql --generate-name
```

The `--generate-name` option is nice!

I think that one should be clear on the definitions of and differences between:

  - chart
  - release

A chart repository contains one or more charts. A chart can have a `CHART VERSION` and an `APP VERSION`. When a chart is installed, a release is created and the release has a name. (This is what `--generate-name` will generate.)


## 🔑 Takeaways

### 🔖 Bookmarks

> While these are from the module, I have found both of these to be super useful already. The former more than the latter.

  - [kubectl Cheat Sheet](https://kubernetes.io/docs/reference/kubectl/cheatsheet/)
  - [Helm Quickstart Guide](https://helm.sh/docs/intro/quickstart/)

## Stage 2