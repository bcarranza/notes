# Advice

Advice that I heard. Take it or leave it. _Think_ about it. 

> The sky is not the limit. 

> Know how to use your voice in conversations. 

> Go high.
