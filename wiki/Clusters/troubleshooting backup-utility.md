> These are just my notes: use the docs, please.

You want the `toolbox` pod.

You can run `backup-utility` by hand: this is useful for testing as you first get backups set up or if you encounter a problem. Long term, you'll want this working [via the `cron` job](https://docs.gitlab.com/charts/backup-restore/backup.html#cron-based-backup). 

  - Get a shell in a `toolbox` pod for this next part
	  - `kubectl get pods -lrelease=RELEASE_NAME,app=toolbox`

```
$ which backup-utility
/usr/local/bin/backup-utility
```


  - https://gitlab.com/gitlab-org/build/CNG/-/blob/master/gitlab-toolbox/scripts/bin/backup-utility

Skipping stuff:

```
/bin/bash /usr/local/bin/backup-utility --skip db --skip registry --skip uploads --skip artifacts --skip lfs --skip packages --skip external_diffs --skip terraform_state --skip ci_secure_files
```


## Docs


  - https://docs.gitlab.com/charts/backup-restore/
  - https://docs.gitlab.com/charts/backup-restore/backup.html


## Issues

> These are issues that I like having quick links to.

  - https://gitlab.com/gitlab-org/charts/gitlab/-/issues/2665


---



[[Kubernetes]]