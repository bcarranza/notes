> Cluster stuff

## OpenShift

OKD - https://www.okd.io/
OKD is

> The Community Distribution of Kubernetes that powers[Red Hat OpenShift](https://www.openshift.com/)

  - [[CI + K8S]]

---

# Security Context Constraints (SCCs)

  - [Managing SCCs in OpenShift](https://cloud.redhat.com/blog/managing-sccs-in-openshift)
	  - See the **Caution**.
	  - There is a new way to do this, see https://examples.openshift.pub/deploy/scc-anyuid/#past-438. 
  - Docs: [Run As `anyuid` SCC](https://docs.gitlab.com/runner/configuration/configuring_runner_operator.html#run-as-anyuid-scc)

---

Kubernetes to OpenShift

The Runner operator is production ready but the GitLab operator is _not_ production-ready. 

OpenShift is opinionated: you can't run _anything_ as root by default. A user ID is assigned to whatever is running (even if you tell it to run as `root`, you'll be ignored). You must rely on the fact that you won't be root. 

OpenShift can be made to respect the user. 


Create a security group, assign it to a service account.
It's rather quite if you get it wrong. (Say you don't specify a namespace...)
