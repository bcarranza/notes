# CI + K8S = <3



## Set up a Runner

```shell
kubectl create namespace cutecats
helm install --namespace cutecats -f values.yaml gitlab-runner gitlab/gitlab-runner --version 0.42.0
```



OK, but what's in `values.yaml`?


```yaml
gitlabUrl: https://gitlab.com
runnerRegistrationToken:  astringgoeshere
replicas: 2
runners:
  name: k8scats
  serviceAccountName:
  tags:

rbac:
  create: true
  resources: ["*"]
  verbs: ["get", "list", "watch", "create", "patch", "delete", "update"]
  serviceAccountAnnotations:
    eks.amazonaws.com/role-arn: "hello"
```


OK, we purposely installed it with an older version of the Chart. Let's upgrade. 


```
helm search repo -l gitlab/gitlab-runner | less
```


See [[Kubernetes]] for a less-summarized version.


Docs: [GitLab Runner Helm Chart](https://docs.gitlab.com/runner/install/kubernetes.html)

## Find out what's wrong

```
FF_PRINT_POD_EVENTS
```

> 	When enabled, all events associated with the build pod will be printed until it’s started.

[GitLab Runner feature flags](https://docs.gitlab.com/runner/configuration/feature-flags.html)
