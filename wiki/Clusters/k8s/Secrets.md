
There are different types of secrets. [TLS secrets](https://kubernetes.io/docs/concepts/configuration/secret/#tls-secrets) make it much easier to configure digital certificates for use within the cluster:

```shell
kubectl create secret tls my-tls-secret \
  --cert=path/to/cert/file \
  --key=path/to/key/file
```

We should be able to use that information to set up a self-signed certificate with our Kubernetes cluster. I like using XCa  or [omgwtfssl](https://github.com/paulczar/omgwtfssl) for this purpose. 

## Updating Secrets

  - Delete and recreate?
	  - This went well. 
  - [Patch](https://stackoverflow.com/questions/55744582/how-to-update-secret-with-kubectl-patch-type-json)?



[[Self-Signed Certificates]]