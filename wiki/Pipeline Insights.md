> An evergreen note of _stuff_ related to Pipeline Insights. I take notes here during the group calls.


## August 2022

  - I [started](https://gitlab.com/gitlab-com/support/support-training/-/issues/2719) working towards becoming the [Support Stable Counterpart](https://about.gitlab.com/handbook/support/support-stable-counterparts.html) for the Pipeline Insights group. I finished this up on August 10th. 
  - List of [Pipeline Insights :bookmark: bookmarks](https://gitlab.com/gitlab-com/support/support-training/-/issues/2719#note_1054273295)


## Issues of Interest
  - [Project artifact storage can become negative](https://gitlab.com/gitlab-org/gitlab/-/issues/368326)

### Testing performance of large artifact uploads

Issue: [Test Performance of "large" build artifact uploads](https://gitlab.com/gitlab-org/gitlab/-/issues/354915)

  - Moved to 15.5, an [upcoming release](https://about.gitlab.com/upcoming-releases/)


## September 2022

We've been thinking and talking about test projects. I decided to start with **Load Performance Testing**. 

  - Docs: [Load Performance Testing](https://docs.gitlab.com/ee/ci/testing/load_performance_testing.html)
  - [[Load Performance Testing]]



## Miscellaneous

[[Support Stable Counterparts]]


  - **Open** [Merge Requests](https://gitlab.com/gitlab-org/gitlab/-/merge_requests?scope=all&state=opened&my_reaction_emoji=bulb) related to Pipeline Insights 💡
  - **All** [Issues](https://gitlab.com/gitlab-org/gitlab/-/issues/?sort=created_date&state=all&my_reaction_emoji=bulb&first_page_size=20) related to Pipeline Insights 💡
  - [[Snippets for Pipeline Insights]]
  - https://mtngs.io/gitlab/pipeline-insights-group/
  - 




