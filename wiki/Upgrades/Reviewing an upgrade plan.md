_This is a work in progress that may be useful nonetheless. This is directed at GitLab Support team members. If you get a `404`: make sure you are signed in via Okta and double-check that you are a GitLab Support team member._

## Quick Questions

  - How has the instance been deployed? (Omnibus, Helm, Docker)
	  - Additional version-specific upgrade instructions and notes are available for Helm-based deployments. 
  - Is Geo in use?
  - Is GitLab CI in use?
	  - If **yes**, are the Runners accounted for? We recommend [matching the major and minor versions](https://docs.gitlab.com/runner/#gitlab-runner-versions) of GitLab and GitLab Runner. (If you're upgrading GitLab to 15.8.Z, upgrade your Runners to 15.8.X.)


## Helpful Links


> I just finished the upgrade. The commands exited successfully. How can I make sure everything is working properly?

Having users use the instance as they normally would is a great test. Before turning the instance over to the users:


  - [Pre-upgrade and post-upgrade checks](https://docs.gitlab.com/ee/update/plan_your_upgrade.html#pre-upgrade-and-post-upgrade-checks)
  - [GitLab Smoke Tests](https://gitlab.com/gitlab-com/support/toolbox/gitlab-smoke-tests) - Use GitLab CI to quickly test if GitLab features are working as intended.
  - Who is [on-call](https://gitlab-com.gitlab.io/support/team/oncall.html)?
	  - Let them know.




