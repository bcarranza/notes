# Team Ops: Guiding Principles

## Shared Reality

  - SSoT
  - Publicy by default
  - Collaboration guidelines
  - Low-context communication
  - Shared values
  - Inclusivity
  - Informal communication

## Everyone contributes

While everyone **can** contribute, everyone is not _required_ to contribute.

  - Asynchronous workflows
  - Give agency (by default!)
    - Agency as the antidote to micromanagement
  - Directly responsible individual
    - DRIs should be empowered to escalate to unblock
  - Push decisions to the lowest possible level
  - Well-managed meetings
    - if it can happen async, it should
  - Key review meetings
  - Short toes
    - Speak up
    - Be comfortable with feedback
    - Don't fear conflict

## Decision Velocity

  - Bias for action
    - "I will" instead of "should I?"
  - Boring solutions 
  - Disagree, commit and disagree 
    - `git disagree && git commit && git disagree`
  - Stable counterparts
  - Collaboration is not consensus
  - Asynchronous Innovation
  - Strong opinions, weakly held
    - Is that an opinion or a decision?

Stable counterparts enhance cross-functional execution without the downsides of a [matrix organization](https://about.gitlab.com/handbook/leadership/#no-matrix-organization).

acting through an agreed-upon structure actually accelerates work rather than stifling it.

### Innovation as a linear process
  1. idea
  1. discussion
  1. implementation
  1. execution

## Measurement Clarity

  - Iteration
  - Definition of done
  - Prioritize due dates over scope
  - Transparent measurements
  - Measure results, not hours
  - Transparent feedback
  - Cadence