# MECC

> Managing so Everyone Can Contribute

_These are notes I took as I worked through the MECC certification in [[2022]]._

Handbook: [MECC](https://about.gitlab.com/handbook/mecc/)


The four pillars of MECC:

### Informed Decisions

The 🔑 key question is (now):

> How does the organization create a system where everyone can consume information (self-serve) and contribute, regardless of level or function?

### Fast Decisions

  - Optimize for speed of knowledge **retrieval**, instead of speed of knowledge transfer

### Many Decisions

  - Bias for action

### Executing (on) Decisions

  - Execution is not a one-time event. 
  - Decisions must be executed on. 


---

MECC aims to create an atmosphwere where _everyone is empowered to lead_. 


  - MECC describes an ideal state. 
  - There are prerequisites for MECC, like:
	  - the right tools
	  - communication guidelines
	  - shared set of values
	  - focus on **Results**
	  - culture of belonging
	  - 

---

# Informed Decisions

## Public by Default
Permit informed decision-making by rejecting "need-to-know" and embracing "public by default" instead. 

> Instead, the onus is to _educate_ team members on how to add information in a systematized manner and how to search for data within the system.

The system must still be used. 

## Single Source of Truth (SSoT)

## Low-context communication

  - explicit over implicit
  - direct over direct
  - simple over complex
  - comprehensive over narrow

## Situational leadership

> It depends. 

## Inclusivity

>In MECC, a [bias for asynchronous communication](https://about.gitlab.com/handbook/values/#bias-towards-asynchronous-communication) fosters [inclusion](https://about.gitlab.com/company/culture/inclusion/). By defaulting to written, asynchronous sharing, everyone contributes in the same size font.


## Shared values


Segue: **Informed** decisions enable **fast** decisions...


# Fast Decisions

## Iteration

## Short toes

## Proposals

Remember that everything is in draft. 

## DRI (Directly responsible individual)

## Informal communication to build trust


## Two-way door decisions

Two-way door decisions should be **easy** to reverse. (Not just _possible_.)


# Many Decisions

## Collaboration is not consensus

Challenge the idea that consenseus is productive, says MECC. 

  - 👍: many small teems iterating quickly (in the open)
  - 👎 large team moving slowly as they work towards consensus


Consider:

  - collaboration allows everyone to **contribute**
  - consensus requires everyone* to **agree**




## Push decisions to the lowest possible level


> Decisions should be made by the person doing the work, not by their manager or their manager's manager. 


## Reduce politics

## Say why, not just what


👁️: this promotes **Transparency** and builds trust. 

Bonus: this is how institutional memory is created -- without needing to rely on the memory of individuals or copies of their inboxes. 

## Asynchronous workflows

## Boring solutions

Simple (and boring) solutions are best. 


## Only healthy constraints




# Executing on Decisions


Above we had the tenets of MECC: here we have **a key output** of MECC. 

  - Give agency
  - Bias for **action**
  - Transparent OKRs and KPIs
  - Iteration enables execution
  - Prioritize due dates over scope
  - 