Docs: [Load Performance Testing](https://docs.gitlab.com/ee/ci/testing/load_performance_testing.html)

Load Performance Testing uses `k6`. 

  - See [k6.io](https://k6.io/) from Grafana Labs! 

### k6

You need a test file. This was a bit weird to me at first but it can be .....a Java script file. 

The [GitLab Performance Tool (GPT)](https://about.gitlab.com/handbook/support/workflows/gpt_quick_start.html) also makes use of `k6`. (OK, lots of things use `k6`. )

  - https://to-be-continuous.gitlab.io/doc/ref/k6/
  - https://gitlab.com/to-be-continuous/k6
  - 

### Example Projects

> Publicly available projects of interest for **Load Performance Testing**. 

  - [bwill/network-alerts-load-test](https://gitlab.com/bwill/network-alerts-load-test)
  - https://github.com/dwp/ms-html-to-pdfa/blob/master/.gitlab-ci.yml


### Example k6 Load Test Files
  - [policy_violation.js](https://gitlab.com/bwill/network-alerts-load-test/-/blob/main/load_test/policy_violation.js)

[performance-test.js](https://k6.io/blog/integrating-load-testing-with-gitlab/#write-your-performance-test-script):

```
import { sleep } from 'k6';
import http from 'k6/http';

export const options = {
  duration: '1m',
  vus: 50,
};

export default function () {
  http.get('http://test.k6.io/contacts.php');
  sleep(3);
}

```


To run load test files like these outside of GitLab CI, you'd use something like:

```
k6 run performance-test.js
```

```
import http from 'k6/http';
export default function () {
  const url = 'http://test.k6.io/login';
  const payload = JSON.stringify({
    email: 'aaaa',
    password: 'bbbb',
    });

    const params = {
      headers: {
          'Content-Type': 'application/json',
          },
        };
    let res=http.post(url, payload, params);
}
```

[source](https://blog.knoldus.com/gitlab/)

gitlab.com/soniya.raichandani/testing


---


When you use the template, the `script` section does something like this:

```
docker run --rm -v "$(pwd)":/k6 -w /k6 $K6_DOCKER_OPTIONS $K6_IMAGE:$K6_VERSION run $K6_TEST_FILE --summary-export=load-performance.json $K6_OPTIONS
```

We see that we're interested in `load-performance.json`. 




https://medium.com/modanisa-engineering/how-to-add-k6-load-test-to-gitlab-ci-pipeline-with-aws-lambda-containers-2096278d5711



## Alternatives

  - [Artillery](https://www.artillery.io/docs/guides/integration-guides/gitlab-ci-cd) , which can be integrated with GitLab CI. 