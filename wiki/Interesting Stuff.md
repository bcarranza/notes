
[Unix legend, who owes us nothing, keeps fixing foundational AWK code](https://arstechnica.com/gadgets/2022/08/unix-legend-who-owes-us-nothing-keeps-fixing-foundational-awk-code/)

- https://github.com/onetrueawk/awk/commit/9ebe940cf3c652b0e373634d2aa4a00b8395b636
- https://github.com/onetrueawk/awk/commit/d322b2b5fc16484affb09e86b044596a2e347853
- Consider reading **The UNIX Programming Environment**
- 