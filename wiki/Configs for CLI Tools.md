
This includes `dotfiles`.

- [brie/dotfiles]()
- bcarranza/dotfiles

## Screen

https://superuser.com/questions/51988/what-is-the-best-gnu-screen-taskbar-youve-used-seen

I borrowed this `hardstatus` from a kind soul on superuser:

```
hardstatus alwayslastline "%{b kw}%H %{r}%1` %{w}| %{g}%c %{w}| %{y}%d.%m.%Y %{w}| %{g}%l %{w}| %{-b kw}%u %-Lw%{= rW}%50> %n%f %t %{-}%+Lw%<"
```


It might be worth [understanding more about GNU screen's hardstatus strings](Understanding GNU Screen’s hardstatus strings)  but having one I like is enough for now. 



## psql

https://psql-tips.org/psql_tips_131.html

From [this HNcomment](https://news.ycombinator.com/item?id=34915157):

```
  -- Show row count of last query in prompt.
  -- Gosh, why did I do it like this...? There was a reason for it and it fixes
  -- something, but I forgot what.
  select :'PROMPT1'='%/%R%x%# ' as default_prompt \gset
  \if :default_prompt
    \set PROMPT1 '(%:ROW_COUNT:)%R%# '
  \endif
  
  \set QUIET                                    \\-- Don't print welcome message etc.
  \set HISTFILE ~/.cache/psql-history- :DBNAME  \\-- Keep history per database
  \set HISTSIZE          -1                     \\-- Infinite history
  \set HISTCONTROL       ignoredups             \\-- Don't store duplicates in history
  \set PROMPT2           '%R%# '                \\-- No database name in the line continuation prompt.
  \set COMP_KEYWORD_CASE lower                  \\-- Complete keywords to lower case.
  \pset linestyle        unicode                \\-- Nicely formatted tables.
  \pset footer           off                    \\-- Don't display "(n rows)" at the end of the table.
  \pset null             'NULL'                 \\-- Display null values as NULL
  \timing                on                     \\-- Show query timings
  
  \set pretty '\\pset numericlocale'            \\-- Toggle between thousands separators in numbers
```