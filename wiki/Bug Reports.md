Interesting bug reports I assembled:

  - [Unexpectedly low vulnerabilities count in group-level Security Dashboard on specific days (August 18th and August 21st, 2022)](https://gitlab.com/gitlab-org/gitlab/-/issues/371932)
  - [Make inclusion of job log excerpts in pipeline failure notification emails configurable instance-wide (per-group, per-project)](https://gitlab.com/gitlab-org/gitlab/-/issues/290306)
  - [Severity and description fields not captured when using IaC Scanning](https://gitlab.com/gitlab-org/gitlab/-/issues/349141)



Alternately, use [this link](https://gitlab.com/gitlab-org/gitlab/-/issues/?sort=popularity&state=opened&author_username=bcarranza&first_page_size=20) to view issues I authored sorted by popularity.



---

https://gitlab.torproject.org/tpo/tpa/gitlab/-/issues/81

