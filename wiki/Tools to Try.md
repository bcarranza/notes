# 🛠️ Tools to Try

> These are tools that someone else excitedly told me about. I haven't tried it yet but I want to keep track of it, for me and for you. 

These are organized by when I added them to the list. I mark them done and leave notes as appropriate. 
  - [x] [watchexec](https://github.com/watchexec/watchexec)
	  - This sounds like an alternative to `inotifywait`. 
	  - LOVE it.
  - [ ] [GitLab Group Member Report](https://gitlab.com/gitlab-com/cs-tools/gitlab-cs-tools/gitlab-group-member-report)
  - [ ] https://github.com/vguhesan/python-hello-world-with-unit-test
  - [x] [Dogsheep](https://dogsheep.github.io/) | No need. Seems neat. 
  - [x] I'm testing out [ticker](https://github.com/achannarasappa/ticker). | 🤷 It's cool but I don't actually care that much about the info it provides.  
  - [ ] https://github.com/eleven-sh/cli
  - [x] The Logfile Navigator: `lnav`
	  - [x] Testing as of January 2023
  - [ ] [log parser](https://github.com/coroot/logparser) and [Coroot Community Edition](https://coroot.com/docs/coroot-community-edition)
	  - [ ] Their [failurepedia](https://coroot.com/failurepedia) is really interesting! 
	  - [ ] [zephyr](https://gitlab.com/shagon/zephyr) for troubleshooting
- [ ] [Integrate Keycloak as an IDP with GitLab](https://github.com/sameersbn/docker-gitlab/blob/master/docs/keycloak-idp.md)
- [ ] Mostly I'm curious about `pipelinestatus` in [this repo](https://gitlab.com/mhenriksen/stuff.git)
- [x] [Simple CLI Pomodoro timer for macOS](https://patloeber.com/pomodoro-app-cli-macos/)
- [ ] [[Geodesic]] seems cool. Can it work as a drop-in replacement for my toolbox container image? 
	-  [docs](https://github.com/cloudposse/geodesic)

## 📆 2024

- [ ] kubebox: [⎈❏ Terminal and Web console for Kubernetes](https://github.com/astefanutti/kubebox) 
- [Yet Another Dotfiles Manager](https://yadm.io/#)

### Actually `glab`

```shell
glab issue list -F urls -R gitlab-com/support/support-pairing --assignee=@me --label=pairing::same-level
```

## The Logfile Navigator

It's cool! It's great.

A few tricks I've picked up:

Use `e`/`E` to move between the next and previous error. 
Use `d` or `D` to move forward or backward 24 hours. Gold!

It's a great general purpose log analysis tool. 

  - [[Using lnav (and friends) to analyze GitLab logs]]

## Ticker

```shell
ticker -w GTLB,FFIV,AAPL,EL,GOOG
```


The [[Toolbox|toolbox]] has things I use and love. 
