# 🐘 Patroni

- https://gitlab.com/gitlab-com/runbooks/-/blob/master/docs/patroni/scripts/parse_patroni.sh
- [Log analysis on PostgreSQL, Pgbouncer, Patroni and consul Runbook](https://gitlab.com/gitlab-com/runbooks/-/blob/master/docs/patroni/log_analysis.md#patronis-log)
- What about mentioning Patroni in the [log rotation](https://docs.gitlab.com/ee/administration/logs/index.html#log-rotation) section of the docs? and elsewhere on that page?
- Explain the services: https://docs.gitlab.com/ee/administration/postgresql/replication_and_failover.html#database-node

## 🏡 3️⃣ 0️⃣0️⃣0️⃣  Reference Architecture

[Reference architecture: Up to 60 RPS or 3,000 users](https://docs.gitlab.com/ee/administration/reference_architectures/3k_users.html)

You'll find the `patroni` service running on PostgreSQL nodes.

## Using `curl`

On a node where `gitlab-ctl status | grep patroni` is `true`:

```
curl -s http://localhost:8008/patroni | jq .
```

```
{
  "state": "running",
  "postmaster_start_time": "2025-01-17 20:10:30.405740+00:00",
  "role": "master",
  "server_version": 140011,
  "xlog": {
    "location": 164356376
  },
  "timeline": 3,
  "replication": [
    {
      "usename": "gitlab_replicator",
      "application_name": "refarch-1k-env-postgres-2",
      "client_addr": "10.128.20.14",
      "state": "streaming",
      "sync_state": "async",
      "sync_priority": 0
    },
    {
      "usename": "gitlab_replicator",
      "application_name": "refarch-1k-env-postgres-3",
      "client_addr": "10.128.20.17",
      "state": "streaming",
      "sync_state": "async",
      "sync_priority": 0
    }
  ],
  "dcs_last_seen": 1737492712,
  "database_system_identifier": "7460596785407278469",
  "patroni": {
    "version": "3.0.1",
    "scope": "postgresql-ha"
  }
}
```


## Using `patronictl`

```
/opt/gitlab/embedded/bin/patronictl -c /var/opt/gitlab/patroni/patroni.yaml
```

See the [upstream docs](https://patroni.readthedocs.io/en/latest/patronictl.html) on `patronictl`.

```
 /opt/gitlab/embedded/bin/patronictl -c /var/opt/gitlab/patroni/patroni.yaml list
```

```
restart --pg-version 14.11 --force postgresql-ha refarch-1k-env-7306595b-postgres-1.c.bcarranza-ad014234.internal
```

🔮 The environment that this data came from has been disposed of and never contained any data.
