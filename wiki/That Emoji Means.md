
> What does that emoji mean?

When I use it:

  - 👀  
	  - I am looking at it. 
  - 🎫
	  - FRT, a brand new ticket. 
  - 🎟
	  - NRT, a ticket that's not **New**. 
  - 🟡/💛/ 🔴
	  - I have no time to look at that. 
	  - That might change later in the day.