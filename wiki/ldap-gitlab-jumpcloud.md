# Using JumpCloud's LDAP offering with GitLab

## Goal

Configure a self-managed GitLab instance to use [**JumpCloud**](https://jumpcloud.com) as an LDAP provider. 

Wanna do this in Kubernetes instead? [[GitLab Helm Chart + JumpCloud LDAP]]
## Setup

  - [ ] Sign up for [jumpcloud.com](https://jumpcloud.com)
  - [ ] Create a JumpCloud user who GitLab will log in as
  - [ ] Create at least one regular user account
  - [ ] _Optional_ Create and populate a group of users who should get **Admin** access
  - [ ] _Optional_ Create and populate a group in JumpCloud of users who should get access to a special group in the GitLab instance
  - [ ] _Optional_ Install `ldapsearch` somewhere (on the GitLab server or on `localhost` would be fine)

### How do I get my Org DN?

Log into the [console](https://console.jumpcloud.com) as the **Administrator**
Browse to **LDAP** > **JumpCloud LDAP** 
On the **Details** tab, you'll see **ORG DN** under **LDAP Instance**. The value will be something like:

```
o=abcdefghijklmno,dc=jumpcloud,dc=com
```

### Set up GitLab

Here's an example omnibus configuration using the **DN** shown above, replace with yours:

```
gitlab_rails['ldap_enabled'] = true
gitlab_rails['prevent_ldap_sign_in'] = false
gitlab_rails['ldap_servers'] = {
'main' => {
  'label' => 'LDAP',
  'host' =>  'ldap.jumpcloud.com',
  'port' => 389,
  'uid' => 'uid',
  'encryption' => 'start_tls',
  'verify_certificates' => true,
  'bind_dn' => 'uid=tanuki,ou=Users,o=abcdefghijklmno,dc=jumpcloud,dc=com',
  'password' => '_the_password_of_the_bind_user',
  'verify_certificates' => true,
  'tls_options' => {
    'ca_file' => '',
    'ssl_version' => '',
    'ciphers' => '',
    'cert' => '',
    'key' => ''
  },
  'timeout' => 10,
  'active_directory' => false,
  'allow_username_or_email_login' => false,
  'block_auto_created_users' => false,
  'base' => 'o=abcdefghijklmno,dc=jumpcloud,dc=com',
  'user_filter' => '',
  'attributes' => {
    'username' => ['uid', 'userid', 'sAMAccountName'],
    'email' => ['mail', 'email', 'userPrincipalName'],
    'name' => 'cn',
    'first_name' => 'givenName',
    'last_name' => 'sn'
  },
  'lowercase_usernames' => false,

  # EE Only
  'group_base' => 'o=abcdefghijklmno,dc=jumpcloud,dc=com',
  'admin_group' => '',
  'external_groups' => [],
  'sync_ssh_keys' => false
  }
}
```

These are the things that you need to modify to suit your environment:

  - `bind_dn` -- Required -- Change **tanuki** to the right username and change **abcdefghijklmno** to _your_ **ORG DN**. This is the user that GitLab will log into the LDAP server as.
    - To get the value for the `bind_dn`, log in to the [Administrator portal](https://console.jumpcloud.com/) for JumpCloud, browse to **LDAP** > **JumpCloud LDAP** and look in **ORG DN** for the value.
  - `password` -- Required -- the password for the user above  
  - `base` -- Required -- Change **abcdefghijklmno** to _your_ **ORG DN**. 
  - `group_base` -- Optional -- If you want to use group sync, change **abcdefghijklmno**  in `group_base` to _your_ **ORG DN**. 
  - `admin_group` -- Optional -- Use if you want JumpCloud users who are members of this group to be automatically made **Admin** in GitLab.  Read about [administrator sync](https://docs.gitlab.com/ee/administration/auth/ldap/#administrator-sync) in the docs. Note that `group_base` is **required** for `admin_group` to work properly. 


## Query via `ldapsearch`

A query like this will show you everything:

```
LDAPTLS_REQCERT=never   ldapsearch -H ldap://ldap.jumpcloud.com:389  -x -D  \
uid=tanuki,ou=Users,o=abcdefghijklmno,dc=jumpcloud,dc=com   -Z     -W    -L   \
-b  ou=Users,o=abcdefghijklmno,dc=jumpcloud,dc=com    > myjumpcloudorg.ldif
```

When using the `-W` switch, you will be prompted for a password. You'll need to change `tanuki` to the a user in your JumpCloud environment and you'll need to change `abcdefghijklmno` to match the **ORG DN** value retrieved earlier above. The resulting `myjumpcloudorg.ldif` file will have information about all LDAP objects. You'll note that the groups that you create in the [JumpCloud console](https://console.jumpcloud.com) will appear as **Users**. Excerpted from the results of `grep '^#' myjumpcloudorg.ldif`: 

```
#
# LDAPv3
# base <ou=Users,o=abcdefghijklmno,dc=jumpcloud,dc=com> with scope subtree
# filter: (objectclass=*)
# requesting: ALL
#
# CoFounders, Users, abcdefghijklmno, jumpcloud.com
# brie, Users, abcdefghijklmno, jumpcloud.com
# All Users, Users, abcdefghijklmno, jumpcloud.com
```

Explore this command with [explainshell](https://explainshell.com/explain?cmd=LDAPTLS_REQCERT%3Dnever+++ldapsearch+-H+ldap%3A%2F%2Fldap.jumpcloud.com%3A389++-x+-D++%5C+uid%3Dtanuki%2Cou%3DUsers%2Co%3Dabcdefghijklmno%2Cdc%3Djumpcloud%2Cdc%3Dcom+++-Z+++++-W++++-L+++%5C+-b++ou%3DUsers%2Co%3Dabcdefghijklmno%2Cdc%3Djumpcloud%2Cdc%3Dcom++++%3E+myjumpcloudorg.ldif). 

## Links

  - [LDAP Configuration](https://docs.gitlab.com/ee/administration/auth/ldap/#configuration) settings for GitLab
  - [[LDAP|Troubleshooting LDAP]]
