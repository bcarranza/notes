
**Get the vulnerability name and file location**

```
jq -r  '.vulnerabilities[] | "\(.message), \(.location.file)"' gl-secret-detection-report\ \(1\).json
```

**Get the vulnerability name and file location including SHA and starting line**

```
jq -r  '.vulnerabilities[] | "\(.message), \(.location)"' gl-secret-detection-report.json
```



## Building your own `jq` Commands


I like using `gron` to get started:

```
gron gl-secret-detection-report.json
```

- [tomnomnom](https://github.com/tomnomnom) / [gron](https://github.com/tomnomnom/gron)

