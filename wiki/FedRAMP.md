
[FedRAMP Initiative](https://www.gsa.gov/technology/government-it-initiatives/fedramp)

## Acronyms

* ATO: Authority to Operate
* CSP: Cloud Service Provider
* PMO: Program Management Office
* RAMP: Risk and Authorization Management Program



- [FedRAMP Agency Authorization Playbook](https://www.fedramp.gov/assets/resources/documents/Agency_Authorization_Playbook.pdf)
- 